// import { Context } from 'koa';
// import { body, path, request, responsesAll, summary, tagsAll } from 'koa-swagger-decorator';
// import {
//   authSchema,
//   loginSchema,
//   User,
//   verifierAdminAuthSchema,
//   verifierIndividualAuthSchema,
// } from '../src/entity/user';
// import { UserService } from '../src/services/UserService';
// import { handleRequestStatus } from '../src/utils/request-status';
// import { generateToken } from '../src/utils/user-utils';
// import { toUserDto } from '../src/dto/user-dto';
// import { sendForgotEmail } from '../src/services/EmailService';
// import { MoreThan } from 'typeorm';
// import * as bcrypt from 'bcrypt';
// import passport from 'koa-passport';
// import crypto from 'crypto';
// import { authValidateSchemas } from '../src/request-validate/authValidateSchema';
// import Joi from 'joi';
// import { AuthService } from '../src/services/AuthService';
// import { responseStatuses } from '../src/constants/controllers';

// export async function generateForgotToken() {
//   return await crypto.randomBytes(20).toString('hex');
// }

// @responsesAll(responseStatuses)
// @tagsAll(['Auth'])
// export default class AuthController {
//   @request('post', '/login')
//   @summary('Login by credentials')
//   @body(loginSchema)
//   public static async login(ctx: Context) {
//     return passport.authenticate('local', (err, user, info, status) => {
//       console.log('User Controller -->', user);
//       if (!user) {
//         if (!status) {
//           status = 401;
//         }
//         return handleRequestStatus(ctx, status, info.message);
//       } else {
//         ctx.body = {
//           token: generateToken(toUserDto(user)),
//           user: toUserDto(user),
//         };
//         ctx.status = 200;
//       }
//     })(ctx);
//   }

//   @request('get', '/logout')
//   @summary('Log out')
//   public static async logOut(ctx: Context) {
//     if (ctx.isAuthenticated()) {
//       ctx.logOut();
//       return handleRequestStatus(ctx, 200, 'Logged out successfully');
//     }
//   }

//   @request('post', '/register')
//   @summary('Register a user')
//   @body(authSchema)
//   public static async register(ctx: Context) {
//     console.log('Body', ctx.request.body);
//     const result = Joi.validate(ctx.request.body, authValidateSchemas.register);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const userService = new UserService();

//     const userToBeSaved: User = new User();
//     const { email, password, data } = ctx.request.body;
//     userToBeSaved.hashedPassword = password;
//     userToBeSaved.email = email;
//     userToBeSaved.data = data;

//     const user = await userService.getBy({ email: userToBeSaved.email });
//     if (user) {
//       const message = 'User already exist';
//       return handleRequestStatus(ctx, 400, message);
//     }

//     try {
//       const user = await userService.create(userToBeSaved);
//       await userService.save(user);
//       const message = 'User created successfully';
//       return handleRequestStatus(ctx, 200, message, toUserDto(user));
//     } catch (e) {
//       return handleRequestStatus(ctx, 500);
//     }
//   }

//   @request('post', '/forgot')
//   @summary('Forgot a user')
//   @body({
//     email: {
//       type: 'string',
//       required: true,
//       example: 'schyles@360id.online',
//     },
//   })
//   public static async forgotPassword(ctx: Context) {
//     const userService = new UserService();
//     const { email } = ctx.request.body;

//     const result = Joi.validate(ctx.request.body, authValidateSchemas.forgot);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const user = await userService.getBy({ email: email });

//     if (!user) {
//       const message = 'The user you are trying to get does not exist';
//       return handleRequestStatus(ctx, 400, message);
//     }

//     user.resetPasswordToken = await generateForgotToken();
//     user.resetPasswordExpire = Date.now() + 3600000;

//     try {
//       await userService.save(user);
//       // TODO: double check the name property
//       // Also, one more reason why to introduce property interface for an entity data [ jsonb ] object
//       const { firstName } = user.data;
//       await sendForgotEmail(user.resetPasswordToken, user.email, firstName);
//       return handleRequestStatus(ctx, 200, 'Mail sent');
//     } catch (e) {
//       return handleRequestStatus(ctx, 500);
//     }
//   }

//   @request('post', '/reset/{token}')
//   @summary('Reset password')
//   @path({
//     token: { type: 'string', required: true, description: 'reset token' },
//   })
//   @body({
//     email: {
//       type: 'string',
//       required: true,
//       example: 'schyles@360id.online',
//     },
//     password: { type: 'string', required: true, example: 'user password' },
//   })
//   public static async resetPassword(ctx: Context) {
//     const token = ctx.params.token;
//     const { password } = ctx.request.body;

//     const result = Joi.validate(ctx.request.body, authValidateSchemas.reset);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const userService = new UserService();

//     const user = await userService.getBy({
//       resetPasswordToken: token,
//       resetPasswordExpire: MoreThan(Date.now()),
//     });
//     if (!user) {
//       const message = 'Password reset token is invalid or has expired.';
//       return handleRequestStatus(ctx, 400, message);
//     }
//     user.hashedPassword = await bcrypt.hash(password, 10);
//     user.resetPasswordToken = undefined;
//     user.resetPasswordExpire = undefined;
//     await userService.save(user);
//     const message = 'Password reset successfully';
//     return handleRequestStatus(ctx, 200, message, toUserDto(user));
//   }

//   @request('post', '/register-admin-verifier/{masterKey}/{businessName}')
//   @summary('Register admin verifier')
//   @path({
//     masterKey: {
//       type: 'string',
//       required: true,
//       description: 'admin master key',
//     },
//     businessName: {
//       type: 'string',
//       required: true,
//       description: 'org business name',
//     },
//   })
//   @body(verifierAdminAuthSchema)
//   public static async registerAdminVerifier(ctx: Context) {
//     const result = Joi.validate(ctx.request.body, authValidateSchemas.registerAdminVerifier);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     return await AuthService.registerAdminVerifier(ctx);
//   }

//   @request('post', '/register-individual-verifier/{individualKey}')
//   @summary('Register individual verifier')
//   @path({
//     individualKey: {
//       type: 'string',
//       required: true,
//       description: 'verifier individual key',
//     },
//   })
//   @body(verifierIndividualAuthSchema)
//   public static async registerIndividualVerifier(ctx: Context) {
//     const result = Joi.validate(ctx.request.body, authValidateSchemas.registerIndividualVerifier);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     return await AuthService.registerIndividualVerifier(ctx);
//   }
// }
