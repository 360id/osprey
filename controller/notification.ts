// import { Context } from 'koa';
// import { body, path, request, responsesAll, summary, tagsAll } from 'koa-swagger-decorator';
// import { handleRequestStatus } from '../utils/request-status';
// import { pagination } from '../utils/pagination';
// import Joi from 'joi';
// import { getRepository } from 'typeorm';
// import { notificationValidateSchema } from '../request-validate/notificationValidateSchema';
// import { NotificationService } from '../services/NotificationService';
// import {
//     Notification,
//     notificationCreateSchema,
//     notificationUpdateSchema,
// } from '../entity/notification';
// import { toNotificationDto } from '../dto/notification-dto';
// import { UserNotification } from '../entity/user-notification';
// import { toUserNotificationDto } from '../dto/user-notification-dto';
// import { createSnsEnpoint, SnsUser } from '../entity/sns-user';
// import { SnsService } from '../services/SnsService';
// import { responseStatuses } from '../constants/controllers';

// @responsesAll(responseStatuses)
// @tagsAll(['Notification'])
// export default class NotificationController {
//     @request('get', '/notifications')
//     @summary('Find all notifications')
//     public static async getNotifications(ctx: Context) {
//         const condition = pagination(ctx);
//         try {
//             const query = getRepository(Notification)
//                 .createQueryBuilder('notification')
//                 .where('notification.deleted = :deleted', { deleted: condition.deleted });

//             query.take(condition.take);
//             query.skip(condition.skip);
//             query.orderBy('notification.createdAt', 'DESC');

//             const notifications: Notification[] = await query.getMany();
//             const parsedNotifications = notifications.map((notification) => {
//                 return toNotificationDto(notification);
//             });
//             ctx.status = 200;
//             ctx.body = parsedNotifications;
//         } catch (e) {
//             return handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('get', '/notifications/{id}')
//     @summary('Find notification by id')
//     @path({
//         id: { type: 'number', required: true, description: 'id of notification' }
//     })
//     public static async getNotification(ctx: Context) {
//         const notificationService = new NotificationService();
//         const id = +ctx.params.id;

//         try {
//             const notification: Notification = await notificationService.getOne(id || 0);

//             if (!notification) {
//                 const message = 'The notification you are trying to get does not exist';
//                 return handleRequestStatus(ctx, 400,  message);
//             }
//             return handleRequestStatus(ctx,  200, '',  toNotificationDto(notification));
//         } catch (e) {
//             return handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('post', '/notification')
//     @summary('Create a notification')
//     @body(notificationCreateSchema)
//     public static async createNotification(ctx: Context) {
//         const result = Joi.validate(ctx.request.body, notificationValidateSchema.createNotification);

//         const { error } = result;
//         const valid = error == undefined;

//         if (!valid) {
//             return handleRequestStatus(ctx, 400, error.details[0].message);
//         }

//         const { messageText, startDate, hasUntilDate, untilDate, audienceType, requestsFrequency} = ctx.request.body;
//         const notificationService = new NotificationService();

//         if (hasUntilDate && !untilDate) {
//             return handleRequestStatus(ctx, 400, 'Until date is required');
//         }

//         let notificationToBeSaved: Notification = new Notification();
//         notificationToBeSaved = {
//             ...notificationToBeSaved,
//             messageText,
//             startDate,
//             hasUntilDate,
//             untilDate,
//             audienceType,
//             requestsFrequency
//         };

//         try {
//             const notification = await notificationService.save(notificationToBeSaved);
//             const message = 'Notification created successfully';
//             return handleRequestStatus(ctx, 200, message, toNotificationDto(notification));
//         } catch (e) {
//             return handleRequestStatus(ctx, 500, 'Invitation email was not send');
//         }
//     }

//     @request('put', '/notifications/{id}')
//     @summary('Update a notification')
//     @path({
//         id: { type: 'number', required: true, description: 'id of notification' }
//     })
//     @body(notificationUpdateSchema)
//     public static async updateNotification(ctx: Context) {

//         const result = Joi.validate(ctx.request.body, notificationValidateSchema.updateNotification);
//         const { error } = result;
//         const valid = error == undefined;

//         if (!valid) {
//             return handleRequestStatus(ctx, 400, error.details[0].message);
//         }

//         const { hasUntilDate, untilDate } = ctx.request.body;

//         if (hasUntilDate && !untilDate) {
//             return handleRequestStatus(ctx, 400, 'Until date is required');
//         }

//         const notificationService = new NotificationService();
//         const id = +ctx.params.id || 0;

//         const notification: Notification =  await notificationService.getOne(id);
//         if (!notification) {
//             const message = 'The notification you are trying to update does not exist';
//             return handleRequestStatus(ctx, 400,  message);
//         }

//         try {
//             await notificationService.update(id, ctx.request.body);
//             const message = 'The notification updated successfully';
//             return handleRequestStatus(ctx, 200, message);
//         } catch (e) {
//             handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('delete', '/notifications/{id}')
//     @summary('Delete notification by id')
//     @path({
//         id: { type: 'number', required: true, description: 'id of notification' }
//     })
//     public static async deleteNotification(ctx: Context) {
//         const notificationService = new NotificationService();
//         const id = +ctx.params.id || 0;
//         const notificationToRemove: Notification = await notificationService.getOne(id);
//         if (!notificationToRemove) {
//             const message = 'The notification you are trying to delete does not exist';
//             return handleRequestStatus(ctx, 400,  message);
//         } else {
//             try {
//                 const message = 'The notification deleted successfully';
//                 await notificationService.delete(id);
//                 return handleRequestStatus(ctx, 200,  message);
//             } catch (e) {
//                 return handleRequestStatus(ctx, 500);
//             }
//         }
//     }

//     @request('get', '/user/notifications')
//     @summary('Find user notifications')
//     public static async getUserNotifications(ctx: Context) {
//         const user = ctx.state.user;
//         const condition = pagination(ctx);
//         try {
//             const query = getRepository(UserNotification)
//                 .createQueryBuilder('userNotification')
//                 .leftJoinAndSelect('userNotification.notification', 'notification')
//                 .where('(notification.deleted = :deleted AND userNotification.userId = :id)', { deleted: condition.deleted, id: user.id })
//                 .take(condition.take)
//                 .skip(condition.skip)
//                 .orderBy('notification.createdAt', 'DESC');

//             const userNotifications: UserNotification[] = await query.getMany();

//             if (!userNotifications) {
//                 return handleRequestStatus(ctx, 200, 'User does not has notifications.');
//             }

//             const parsedUserNotifications = userNotifications.map((userNotification) => {
//                     return toUserNotificationDto(userNotification);
//             });

//             ctx.status = 200;
//             ctx.body = parsedUserNotifications;
//         } catch (e) {
//             return handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('post', '/create-sns-endpoint')
//     @summary('Create sns endpoint')
//     @body(createSnsEnpoint)
//     public static async createSnsEndpoint(ctx: Context) {
//         const user = ctx.state.user;
//         const { deviceId, token, platform } = ctx.request.body;
//         try {
//             const snsUser = await getRepository(SnsUser)
//                 .createQueryBuilder('snsUser')
//                 .select('snsUser.pushToken', 'pushToken')
//                 .addSelect('snsUser.userId', 'userId')
//                 .addSelect('snsUser.id', 'id')
//                 .addSelect('snsUser.awsEndpoint', 'awsEndpoint')
//                 .where('(snsUser.deviceId = :device AND snsUser.userId = :userId)', {device: deviceId, userId: user.id})
//                 .andWhere('snsUser.deleted = :deleted', {deleted: false})
//                 .getRawOne();

//             if (!snsUser) {
//                 await SnsService.createPlatformEndpointForDevice(user, deviceId, token, platform);
//                 return handleRequestStatus(ctx, 200);
//             }

//             if (snsUser) {
//                 if (token !== snsUser.pushToken) {
//                     const data = await SnsService.deleteEndpoint(snsUser);
//                     if (data) {
//                         await getRepository(SnsUser).delete(snsUser.id);
//                     }
//                     await SnsService.createPlatformEndpointForDevice(user, deviceId, token, platform);
//                     return handleRequestStatus(ctx, 200);
//                 }
//                 return handleRequestStatus(ctx, 200);
//             }
//         } catch (e) {
//             return handleRequestStatus(ctx, 500, 'Something went wrong');
//         }
//     }

//     @request('post', '/push-notification')
//     @summary('Create sns endpoint')
//     public static async pushNotification(ctx: Context) {
//         const user = ctx.state.user;
//         const { deviceId, message, platform } = ctx.request.body;
//         try {
//             const snsUser: SnsUser = await getRepository(SnsUser)
//                 .createQueryBuilder('snsUser')
//                 .select('snsUser.awsEndpoint', 'awsEndpoint')
//                 .where('(snsUser.deviceId = :device AND snsUser.userId = :userId)', {device: deviceId, userId: user.id})
//                 .getRawOne();

//             if (snsUser) {
//                 await SnsService.publishNotification(snsUser.awsEndpoint, platform, message);
//                 return handleRequestStatus(ctx, 200);
//             }
//         } catch (e) {
//             return handleRequestStatus(ctx, 500, 'Something went wrong');
//         }
//     }
// }
