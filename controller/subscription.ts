// import { Context } from 'koa';
// import { body, path, request, responsesAll, summary, tagsAll } from 'koa-swagger-decorator';
// import { Subscription } from '../entity/subscription';
// import { SubscriptionService } from '../services/SubscriptionService';
// import { OrganizationSubscriptionService } from '../services/OrganizationSubscription';
// import { handleRequestStatus } from '../utils/request-status';
// import { OrganizationSubscription } from '../entity/organziation-subscribtion';
// import { OrganizationService } from '../services/OrganizationService';
// import { Organization } from '../entity/organization';
// import { responseStatuses } from '../constants/controllers';

// @responsesAll(responseStatuses)
// @tagsAll(['Subscription'])
// export default class SubscriptionController {

//     @request('delete', '/cancel-subscription')
//     @summary('Cancel subscriptions')
//     public static async cancelSubscription(ctx: Context) {
//         const user = ctx.state.user;
//         try {
//             await SubscriptionService.canceleSubscription(user.organizationId);
//         } catch (e) {
//             return handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('post', '/webhooks')
//     @summary('create a stripe subscription')
//     public static async getWebhook(ctx: Context) {
//         let event;
//         try {
//             event = ctx.request.body;
//         } catch (err) {
//             return handleRequestStatus(ctx, 403, 'Something went wrong');
//         }
//         const subscriptionService = new SubscriptionService();
//         const subscription = new Subscription();
//         const organizationSubscriptionService = new OrganizationSubscriptionService();
//         const subscriptionOrganization = new OrganizationSubscription();
//         const organizationService = new OrganizationService();

//         switch (event.type) {
//             case 'customer.subscription.created':
//                 const organization: Organization = await organizationService.getOne(event.data.object.metadata.organizationId);
//                 if (!organization) {
//                     return handleRequestStatus(ctx, 403, 'Organization dose not exist');
//                 }
//                 subscription.stripeCustomerId = event.data.object.customer;
//                 organization.stripeSubscriptionId = event.data.object.id;
//                 organizationService.save(organization);
//                 subscription.isActive = true;
//                 subscriptionService.save(subscription);
//                 subscriptionOrganization.subscription = subscription;
//                 subscriptionOrganization.organization = organization;
//                 organizationSubscriptionService.save(subscriptionOrganization);
//                 return handleRequestStatus(ctx, 200, 'Subscription created successfully');
//             case 'customer.subscription.deleted':
//                 return handleRequestStatus(ctx, 200, 'Subscription canceled successfully');
//         }
//     }
// }
