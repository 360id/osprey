// import { Context } from 'koa';
// import { body, path, request, responsesAll, summary, tagsAll } from 'koa-swagger-decorator';
// import { handleRequestStatus } from '../utils/request-status';
// import { toScanDocumentDto } from '../dto/scan-document-dto';
// import { pagination } from '../utils/pagination';
// import { createScanDocument, updateScanDocument } from '../entity/scan-document';
// import { ScanService } from '../services/ScanService';
// import RapidIdService, { DriverLicense, Passport } from '../services/RapidIdService';
// import { responseStatuses } from '../constants/controllers';

// @responsesAll(responseStatuses)
// @tagsAll(['ScanDocument'])
// export default class ScanDocument {
//     @request('get', '/scanDocuments')
//     @summary('Find all scan documents')
//     public static async getScanDocuments(ctx: Context) {
//         try {
//             const condition = pagination(ctx);
//             const {skip, take, deleted} = condition || {};
//             const scanService = new ScanService();

//             const scanDocumentsList = await scanService.getList({skip, take, deleted});

//             const parsedScanDocuments = scanDocumentsList.map((scanDocument) => {
//                 return toScanDocumentDto(scanDocument);
//             });
//             ctx.status = 200;
//             ctx.body = parsedScanDocuments;
//         } catch (e) {
//             return handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('get', '/scanDocuments/{id}')
//     @summary('Find scan document by id')
//     @path({
//         id: {type: 'number', required: true, description: 'id of scan document'}
//     })
//     public static async getScanDocument(ctx: Context) {
//         try {
//             const scanService = new ScanService();
//             const id = +ctx.params.id || 0;

//             const scanDocument = await scanService.getOne(id);

//             if (!scanDocument) {
//                 const message = 'The document you are trying to get does not exist';
//                 return handleRequestStatus(ctx, 400, message);
//             }

//             return handleRequestStatus(ctx, 200, '', toScanDocumentDto(scanDocument));
//         } catch (e) {
//             return handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('post', '/scanDocuments')
//     @summary('Create a scan document')
//     @body(createScanDocument)
//     public static async createScanDocument(ctx: Context) {
//         try {
//             if (!ctx.request.body) {
//                 return;
//             }
//             const data = {
//                 ...ctx.request.body,
//                 id: ctx.request.body.jumioIdScanReference
//             };

//             if (data.idScanStatus === 'SUCCESS') {
//                 let rapidIdVerification = {};
//                 const rapidIdService = new RapidIdService();
//                 if (data.idType === 'DRIVING_LICENSE') {
//                     const drivingLicenseData: DriverLicense = {
//                         BirthDate: data.idDob,
//                         GivenName: data.idFirstName,
//                         FamilyName: data.idLastName,
//                         LicenceNumber: data.registrationNumber,
//                         StateOfIssue: data.issuingPlace,
//                         ExpiryDate: data.idExpiry,
//                     };
//                     rapidIdVerification = await rapidIdService.verifyDriversLicence(drivingLicenseData);
//                 } else if (data.idType === 'PASSPORT') {
//                     const passportData: Passport = {
//                         BirthDate: data.idDob,
//                         GivenName: data.idFirstName,
//                         FamilyName: data.idLastName,
//                         MiddleName: data.idMiddleName || '',
//                         TravelDocumentNumber: data.registrationNumber,
//                         Gender: data.gender,
//                         ExpiryDate: data.idExpiry,
//                     };
//                     rapidIdVerification = await rapidIdService.verifyPassport(passportData);
//                 }

//                 data.rapidIdVerification = rapidIdVerification;
//             }

//             const scanService = new ScanService();
//             await scanService.create(data);

//             return handleRequestStatus(ctx, 200, '');
//         } catch (e) {
//             return handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('put', '/scanDocuments/{id}')
//     @summary('Update a scan document')
//     @path({
//         id: { type: 'number', required: true, description: 'id of scan document' }
//     })
//     @body(updateScanDocument)
//     public static async updateScanDocument(ctx: Context) {
//         const scanService = new ScanService();
//         const id = +ctx.params.id || 0;

//         const scanDocument: ScanDocument = await scanService.getOne(id);
//         if (!scanDocument) {
//             const message = 'The scan document you are trying to update does not exist';
//             return handleRequestStatus(ctx, 400, message);
//         }

//         try {
//             await scanService.update(id, ctx.request.body);
//             const message = 'The scan document updated successfully';
//             return handleRequestStatus(ctx, 200, message);
//         } catch (e) {
//             handleRequestStatus(ctx, 500);
//         }
//     }

//     @request('delete', '/scanDocuments/{id}')
//     @summary('Delete scan document by id')
//     @path({
//         id: {type: 'number', required: true, description: 'id of scan document'}
//     })
//     public static async deleteScanDocument(ctx: Context) {
//         const scanService = new ScanService();
//         const id = +ctx.params.id || 0;

//         const scanDocumentToRemove = await scanService.getOne(id);

//         if (!scanDocumentToRemove) {
//             const message = 'The scan document you are trying to delete does not exist';
//             return handleRequestStatus(ctx, 400, message);
//         } else {
//             try {
//                 await scanService.delete(id);
//                 const message = 'The scan document deleted successfully';
//                 return handleRequestStatus(ctx, 200, message);
//             } catch (e) {
//                 return handleRequestStatus(ctx, 500);
//             }
//         }
//     }

// }
