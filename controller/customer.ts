// import { Context } from 'koa';
// import { body, path, request, responsesAll, summary, tagsAll } from 'koa-swagger-decorator';
// import { createUserSchema, supportBody, updateUserSettings, User, UserRole } from '../entity/user';
// import { UserService } from '../services/UserService';
// import { handleRequestStatus } from '../utils/request-status';
// import { toUserDto } from '../dto/user-dto';
// import { pagination } from '../utils/pagination';
// import { userValidateSchema } from '../request-validate/userValidateSchema';
// import Joi from 'joi';
// import { Brackets, getRepository } from 'typeorm';
// import { updateUserSchema } from '../entity/user';
// import { responseStatuses } from '../constants/controllers';
// import { query } from 'koa-swagger-decorator/dist';
// import { sendSupportEmail, getToEmailAdress, ToEmail, sendUserCreateEmail } from '../services/EmailService';

// @responsesAll(responseStatuses)
// @tagsAll(['Customer'])
// export default class CustomerController {
//   @request('get', '/customers')
//   @summary('Find all customers')
//   @query({
//     name: { type: 'string', description: 'name' }
//   })
//   public static async getCustomers(ctx: Context) {
//     const { name } = ctx.request.query;
//     const condition = pagination(ctx);
//     try {
//       const query = getRepository(User)
//         .createQueryBuilder('user')
//         .where('(user.deleted = :deleted AND user.role = :role)', { deleted: condition.deleted, role: UserRole.USER });

//       if (name) {
//         query.andWhere(
//           new Brackets((qb) => {
//             qb.where("data->>'firstName' ILIKE :data", { data: '%' + name + '%' }).orWhere("data->>'lastName' ILIKE :data", {
//               data: '%' + name + '%'
//             });
//           })
//         );
//       }

//       query.take(condition.take);
//       query.skip(condition.skip);
//       query.orderBy('user.createdAt', 'DESC');

//       const users: User[] = await query.getMany();
//       const parsedUsers = users.map((user) => {
//         return toUserDto(user);
//       });
//       ctx.status = 200;
//       ctx.body = parsedUsers;
//     } catch (e) {
//       return handleRequestStatus(ctx, 500);
//     }
//   }

//   @request('get', '/customers/{id}')
//   @summary('Find customer by id')
//   @path({
//     id: { type: 'number', required: true, description: 'id of user' }
//   })
//   public static async getCustomer(ctx: Context) {
//     const userService = new UserService();
//     const id = +ctx.params.id;

//     try {
//       const user: User = await userService.getOne(id || 0);

//       if (!user) {
//         const message = 'The customer you are trying to get does not exist';
//         return handleRequestStatus(ctx, 400, message);
//       }
//       return handleRequestStatus(ctx, 200, '', toUserDto(user));
//     } catch (e) {
//       return handleRequestStatus(ctx, 500);
//     }
//   }

//   @request('put', '/customers/{id}')
//   @summary('Update a customer')
//   @path({
//     id: { type: 'number', required: true, description: 'id of user' }
//   })
//   @body(updateUserSchema)
//   public static async updateCustomer(ctx: Context) {
//     const userService = new UserService();
//     const id = +ctx.params.id || 0;

//     const result = Joi.validate(ctx.request.body, userValidateSchema.updateUser);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const user: User = await userService.getOne(id);
//     if (!user) {
//       const message = 'The customer you are trying to update does not exist';
//       return handleRequestStatus(ctx, 400, message);
//     }

//     try {
//       await userService.update(id, ctx.request.body);
//       const message = 'The customer updated successfully';
//       return handleRequestStatus(ctx, 200, message);
//     } catch (e) {
//       handleRequestStatus(ctx, 500);
//     }
//   }

//   @request('delete', '/customers/{id}')
//   @summary('Delete customer by id')
//   @path({
//     id: { type: 'number', required: true, description: 'id of customer' }
//   })
//   public static async deleteCustomer(ctx: Context) {
//     const userService = new UserService();
//     const id = +ctx.params.id || 0;
//     const userToRemove: User = await userService.getOne(id);
//     if (!userToRemove) {
//       const message = 'The customer you are trying to delete does not exist';
//       return handleRequestStatus(ctx, 400, message);
//     } else {
//       try {
//         const message = 'The customer deleted successfully';
//         await userService.delete(id);
//         return handleRequestStatus(ctx, 200, message);
//       } catch (e) {
//         return handleRequestStatus(ctx, 500);
//       }
//     }
//   }

//   @request('post', '/settings')
//   @summary('Update settings')
//   @body(updateUserSettings)
//   public static async updateSettings(ctx: Context) {
//     const result = Joi.validate(ctx.request.body, userValidateSchema.updateSettings);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//         return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const user = ctx.state.user;
//     const userService = new UserService();
//     try {
//         await userService.update(user.id, ctx.request.body);
//         const message = 'The customer settings updated successfully';
//         return handleRequestStatus(ctx, 200, message);
//     } catch (e) {
//         handleRequestStatus(ctx, 500);
//     }
//   }

//     @request('post', '/create-customer')
//     @summary('Create a customer')
//     @body(createUserSchema)
//     public static async createCustomer(ctx: Context) {
//         const result = Joi.validate(ctx.request.body, userValidateSchema.createUser);

//         const { error } = result;
//         const valid = error == undefined;

//         if (!valid) {
//             return handleRequestStatus(ctx, 400, error.details[0].message);
//         }

//         const { email, data } = ctx.request.body;
//         const userService = new UserService();
//         const user = await userService.getBy({ email: email });

//         if (user) {
//             const message = 'User email already exist';
//             return handleRequestStatus(ctx, 400, message);
//         }
//         const userToBeSaved: User = new User();
//         userToBeSaved.email = email;
//         userToBeSaved.data = data;
//         userToBeSaved.role = UserRole.USER;
//         try {
//             await userService.save(userToBeSaved);
//             await sendUserCreateEmail(email);
//             return handleRequestStatus(ctx, 200, 'Invitation email was sent');
//         } catch (e) {
//             return handleRequestStatus(ctx, 500, 'Invitation email was not send');
//         }
//     }

//   @request('post', '/customer-support')
//   @summary('Send support email settings')
//   @body(supportBody)
//   public static async customerSupport(ctx: Context) {
//       const result = Joi.validate(ctx.request.body, userValidateSchema.support);
//       const { error } = result;
//       const valid = error == undefined;

//       if (!valid) {
//           return handleRequestStatus(ctx, 400, error.details[0].message);
//       }
//       const user = ctx.state.user;
//       const { subject, emailMessage, userEmail } = ctx.request.body;
//       try {
//           const toEmail: ToEmail = await getToEmailAdress(user);
//           await sendSupportEmail(emailMessage, subject, userEmail, toEmail.email);
//           const message = 'Support mail was sent.';
//           return handleRequestStatus(ctx, 200, message);
//       } catch (e) {
//           handleRequestStatus(ctx, 500);
//       }
//   }

// }
