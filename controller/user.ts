// import { Context } from 'koa';
// import { body, path, request, responsesAll, summary, tagsAll } from 'koa-swagger-decorator';
// import { User, UserRole } from '../entity/user';
// import { UserService } from '../services/UserService';
// import { handleRequestStatus } from '../utils/request-status';
// import { toUserDto } from '../dto/user-dto';
// import { pagination } from '../utils/pagination';
// import { userValidateSchema } from '../request-validate/userValidateSchema';
// import Joi from 'joi';
// import { sendMasterKeyEmail } from '../services/EmailService';
// import { Brackets, getRepository } from 'typeorm';
// import { createUserSchema, updateUserSchema } from '../entity/user';
// import uniqueString from 'unique-string';
// import { query } from 'koa-swagger-decorator/dist';
// import { jsonToExel } from '../components/xls/jsonToXls';
// import { responseStatuses } from '../constants/controllers';

// @responsesAll(responseStatuses)
// @tagsAll(['User'])
// export default class UserController {
//   @request('get', '/users')
//   @summary('Find all users')
//   @query({
//     name: { type: 'string', description: 'name' }
//   })
//   public static async getUsers(ctx: Context) {
//     const { organizationId } = ctx.state.user;
//     const { name } = ctx.request.query;
//     const condition = pagination(ctx);
//     try {
//       const query = getRepository(User).createQueryBuilder('user').where('user.deleted = :deleted', { deleted: condition.deleted });

//       if (organizationId) {
//         query.andWhere('user.organizationId = :organizationId', { organizationId: organizationId });
//       }

//       if (name) {
//         query.andWhere(
//           new Brackets((qb) => {
//             qb.where("data->>'firstName' ILIKE :data", { data: '%' + name + '%' }).orWhere("data->>'lastName' ILIKE :data", {
//               data: '%' + name + '%'
//             });
//           })
//         );
//       }

//       query.take(condition.take);
//       query.skip(condition.skip);
//       query.orderBy('user.createdAt', 'DESC');

//       const users: User[] = await query.getMany();
//       const parsedUsers = users.map((user) => {
//         return toUserDto(user);
//       });
//       ctx.status = 200;
//       ctx.body = parsedUsers;
//     } catch (e) {
//       return handleRequestStatus(ctx, 500);
//     }
//   }

//   @request('get', '/users/{id}')
//   @summary('Find user by id')
//   @path({
//     id: { type: 'number', required: true, description: 'id of user' }
//   })
//   public static async getUser(ctx: Context) {
//     const userService = new UserService();
//     const id = +ctx.params.id;

//     try {
//       const user: User = await userService.getOne(id || 0);

//       if (!user) {
//         const message = 'The user you are trying to get does not exist';
//         return handleRequestStatus(ctx, 400, message);
//       }
//       return handleRequestStatus(ctx, 200, '', toUserDto(user));
//     } catch (e) {
//       return handleRequestStatus(ctx, 500);
//     }
//   }

//   @request('post', '/users')
//   @summary('Create a user')
//   @body(createUserSchema)
//   public static async createUser(ctx: Context) {
//     const result = Joi.validate(ctx.request.body, userValidateSchema.createUser);

//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const { email, data } = ctx.request.body;
//     const userService = new UserService();
//     const user = await userService.getBy({ email: email });

//     if (user) {
//       const message = 'User email already exist';
//       return handleRequestStatus(ctx, 400, message);
//     }
//     const userToBeSaved: User = new User();
//     userToBeSaved.email = email;
//     userToBeSaved.data = data;
//     userToBeSaved.role = UserRole.INDIVIDUAL_VERIFIER;
//     userToBeSaved.individualKey = uniqueString();
//     userToBeSaved.organizationId = ctx.state.user.organizationId;

//     try {
//       const user = await userService.save(userToBeSaved);
//       await sendMasterKeyEmail(user.individualKey, email, '', false);
//       return handleRequestStatus(ctx, 200, 'Invitation email was sent');
//     } catch (e) {
//       return handleRequestStatus(ctx, 500, 'Invitation email was not send');
//     }
//   }

//   @request('put', '/users/{id}')
//   @summary('Update a user')
//   @path({
//     id: { type: 'number', required: true, description: 'id of user' }
//   })
//   @body(updateUserSchema)
//   public static async updateUser(ctx: Context) {
//     const userService = new UserService();
//     const id = +ctx.params.id || 0;

//     const result = Joi.validate(ctx.request.body, userValidateSchema.updateUser);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const user: User = await userService.getOne(id);
//     if (!user) {
//       const message = 'The user you are trying to update does not exist';
//       return handleRequestStatus(ctx, 400, message);
//     }

//     try {
//       await userService.update(id, ctx.request.body);
//       const message = 'The user updated successfully';
//       return handleRequestStatus(ctx, 200, message);
//     } catch (e) {
//       handleRequestStatus(ctx, 500);
//     }
//   }

//   @request('delete', '/users/{id}')
//   @summary('Delete user by id')
//   @path({
//     id: { type: 'number', required: true, description: 'id of user' }
//   })
//   public static async deleteUser(ctx: Context) {
//     const userService = new UserService();
//     const id = +ctx.params.id || 0;
//     const userToRemove: User = await userService.getOne(id);
//     if (!userToRemove) {
//       const message = 'The user you are trying to delete does not exist';
//       return handleRequestStatus(ctx, 400, message);
//     } else {
//       try {
//         const message = 'The user deleted successfully';
//         await userService.delete(id);
//         return handleRequestStatus(ctx, 200, message);
//       } catch (e) {
//         return handleRequestStatus(ctx, 500);
//       }
//     }
//   }

//   @request('get', '/profile')
//   @summary('Get Profile')
//   public static async getProfile(ctx: Context) {
//     const user = ctx.state.user;
//     if (!user) {
//       const message = 'The user you are trying to get does not exist';
//       return handleRequestStatus(ctx, 400, message);
//     }
//     return handleRequestStatus(ctx, 200, '', toUserDto(user));
//   }

//     @request('post', '/download-xlsx')
//     @summary('Download xlsx')
//     public static downloadXlsx(ctx: Context) {
//         ctx.status = 200;
//         const stream = jsonToExel([]);
//         ctx.res.writeHead(200, {
//             'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
//             'Access-Control-Allow-Origin': '*',
//             'Content-Disposition': `attachment; filename=test.xlsx`
//         });

//         ctx.res.end(stream);
//     }
// }
