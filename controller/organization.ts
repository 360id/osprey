// import { Context } from 'koa';
// import { body, path, request, responsesAll, summary, tagsAll } from 'koa-swagger-decorator';
// import { Organization, orgSchema } from '../entity/organization';
// import { UserService } from '../services/UserService';
// import { User, UserRole } from '../entity/user';
// import { OrganizationService } from '../services/OrganizationService';
// import Joi from 'joi';
// import { orgValidateSchema } from '../request-validate/orgValidateSchema';
// import { toOrganizationDto } from '../dto/organization-dto';
// import { pagination } from '../utils/pagination';
// import { handleRequestStatus } from '../utils/request-status';
// import { Brackets, Equal, getRepository, Not } from 'typeorm';
// import { sendMasterKeyEmail } from '../services/EmailService';
// import { organization } from './index';
// import { toUserDto } from '../dto/user-dto';
// import { Status } from '../entity/base-entity';
// import { query } from 'koa-swagger-decorator/dist';
// import { responseStatuses } from '../constants/controllers';

// export interface OrgUpdate {
//   businessName?: string;
//   data?: object;
//   status: string;
// }

// @responsesAll(responseStatuses)
// @tagsAll(['Organization'])
// export default class OrganizationController {
//   @request('post', '/organization')
//   @summary('Create organization')
//   @body(orgSchema)
//   public static async createOrganization(ctx: Context) {
//     const result = Joi.validate(ctx.request.body, orgValidateSchema.createOrg);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const adminOrg: User = new User();
//     const userService = new UserService();
//     const { businessName, status, masterKey, data } = ctx.request.body;

//     const user = await userService.getBy({ email: data.email });
//     if (user) {
//       const message = 'User already exist';
//       return handleRequestStatus(ctx, 400, message);
//     }

//     try {
//       const organizationService = new OrganizationService();
//       const isExistOrg = await organizationService.getBy({
//         where: [{ businessName: businessName }, { masterKey: masterKey }]
//       });

//       if (isExistOrg) {
//         const message = 'Organization name or master key already taken';
//         return handleRequestStatus(ctx, 400, message);
//       }
//       adminOrg.data = {
//         firstName: data.firstName,
//         lastName: data.lastName
//       };
//       adminOrg.email = data.email;
//       adminOrg.role = UserRole.ADMIN_VERIFIER;
//       adminOrg.status = status;
//       adminOrg.masterKey = masterKey;
//       const createdUser = await userService.save(adminOrg);
//       let organizationToBeSaved: Organization = new Organization();
//       organizationToBeSaved = {
//         ...organizationToBeSaved,
//         masterKey,
//         status,
//         data,
//         businessName,
//         owner: createdUser
//       };
//       const createdOrganization = await organizationService.save(organizationToBeSaved);
//       createdUser.organizationId = createdOrganization;
//       await userService.save(createdUser);
//       if (status === Status.INACTIVE) {
//         try {
//           await sendMasterKeyEmail(createdOrganization.masterKey, createdUser.email, createdOrganization.businessName, true);
//           return handleRequestStatus(ctx, 200, 'Invitation email was sent');
//         } catch (e) {
//           return handleRequestStatus(ctx, 500, 'Mail was not send');
//         }
//       } else {
//         const message = 'Organization was created';
//         return handleRequestStatus(ctx, 200, message, toOrganizationDto(createdOrganization));
//       }
//     } catch (e) {
//       return handleRequestStatus(ctx, 500, 'Something went wrong');
//     }
//   }

//   @request('get', '/organizations')
//   @summary('Get all organization')
//   @query({
//     businessName: { type: 'string', description: 'businessName' }
//   })
//   public static async getOrganizations(ctx: Context) {
//     const { businessName } = ctx.request.query;
//     const condition = pagination(ctx);
//     try {
//       const query = getRepository(Organization)
//         .createQueryBuilder('organization')
//         .leftJoinAndSelect('organization.users', 'user', 'user.role = :role')
//         .setParameter('role', UserRole.ADMIN_VERIFIER)
//         .where('organization.deleted = :deleted', { deleted: condition.deleted });

//       if (businessName) {
//         query.andWhere('organization.businessName ILIKE :businessName', { businessName: '%' + businessName + '%' });
//       }

//       query.addOrderBy('organization.createdAt', 'DESC');
//       query.take(condition.take);
//       query.skip(condition.skip);

//       const orgs = await query.getMany();

//       const parsedOrgs = orgs.map((organization) => {
//         return toOrganizationDto(organization);
//       });

//       ctx.status = 200;
//       ctx.body = parsedOrgs;
//     } catch (e) {
//       return handleRequestStatus(ctx, 500, 'Something went wrong');
//     }
//   }

//   @request('get', '/organizations/{id}')
//   @summary('Find organization by id')
//   @path({
//     id: { type: 'number', required: true, description: 'Id of organization' }
//   })
//   public static async getOrganization(ctx: Context) {
//     const orgId = +ctx.params.id || 0;

//     try {
//       const organizationService = new OrganizationService();
//       const organization = await organizationService.getOne(orgId);

//       const toDtoOrg = toOrganizationDto(organization);

//       if (!organization) {
//         const message = 'The organization you are trying to get not exist';
//         return handleRequestStatus(ctx, 400, message);
//       }
//       return handleRequestStatus(ctx, 200, '', toDtoOrg);
//     } catch (e) {
//       return handleRequestStatus(ctx, 500, 'Something went wrong');
//     }
//   }

//   @request('get', '/organization-users/{orgId}')
//   @summary('Find organization users by orgId')
//   @path({
//     orgId: { type: 'number', required: true, description: 'Id of organization' }
//   })
//   @query({
//     name: { type: 'string', description: 'name' }
//   })
//   public static async getOrgUsers(ctx: Context) {
//     const orgId = +ctx.params.id;
//     const { name } = ctx.request.query;

//     try {
//       const query = getRepository(User).createQueryBuilder('user').where('user.organizationId = :id', { id: orgId });
//       if (name) {
//         query.andWhere(
//           new Brackets((qb) => {
//             qb.where("data->>'firstName' ILIKE :data", { data: '%' + name + '%' }).orWhere("data->>'lastName' ILIKE :data", {
//               data: '%' + name + '%'
//             });
//           })
//         );
//       }

//       query.andWhere('user.deleted = :deleted', {
//         deleted: false
//       });

//       const users = await query.getMany();
//       if (!users) {
//         const message = 'The organization does not have users';
//         return handleRequestStatus(ctx, 400, message);
//       }
//       const parsedUsers = users.map((user) => {
//         return toUserDto(user);
//       });

//       const individualVerifiers = parsedUsers.filter((user) => user.role === UserRole.INDIVIDUAL_VERIFIER);
//       const adminVerifier = parsedUsers.filter((user) => user.role === UserRole.ADMIN_VERIFIER);
//       const orgUsersData = {
//         orgUsers: parsedUsers,
//         totalMembers: parsedUsers.length,
//         individualVerifierCount: individualVerifiers.length,
//         adminVerifierCount: adminVerifier.length
//       };
//       return handleRequestStatus(ctx, 200, '', orgUsersData);
//     } catch (e) {
//       return handleRequestStatus(ctx, 500, 'Something went wrong');
//     }
//   }

//   @request('put', '/organizations/{id}')
//   @summary('Update  organization')
//   @path({
//     id: { type: 'number', required: true, description: 'id of organization' }
//   })
//   @body(orgSchema)
//   public static async updateOrganization(ctx: Context) {
//     const result = Joi.validate(ctx.request.body, orgValidateSchema.updateOrg);
//     const { error } = result;
//     const valid = error == undefined;

//     if (!valid) {
//       return handleRequestStatus(ctx, 400, error.details[0].message);
//     }

//     const organizationService = new OrganizationService();
//     const id = +ctx.params.id || 0;

//     const organization: Organization = await organizationService.getOne(id);
//     if (!organization) {
//       const message = 'The organization you are trying to update  not exist';
//       return handleRequestStatus(ctx, 400, message);
//     }

//     const { businessName, status, data } = ctx.request.body;

//     const isExistOrg = await organizationService.getBy({ businessName: businessName, id: Not(Equal(id)) });

//     if (isExistOrg) {
//       const message = 'Organization name already taken';
//       return handleRequestStatus(ctx, 400, message);
//     }

//     const organizationToBeUpdate: OrgUpdate = {
//       data: data,
//       status: status
//     };

//     if (businessName) {
//       organizationToBeUpdate.businessName = businessName;
//     }

//     try {
//       await organizationService.update(id, organizationToBeUpdate);
//       const message = 'The organization updated successfully';
//       return handleRequestStatus(ctx, 200, message);
//     } catch (e) {
//       handleRequestStatus(ctx, 500, 'Something went wrong');
//     }
//   }

//   @request('delete', '/organizations/{id}')
//   @summary('Delete organization by id')
//   @path({
//     id: { type: 'number', required: true, description: 'id of organization' }
//   })
//   public static async deleteOrganization(ctx: Context) {
//     const organizationService = new OrganizationService();
//     const id = +ctx.params.id || 0;
//     const organizationToRemove: User = await organizationService.getOne(id);
//     if (!organizationToRemove) {
//       const message = 'The organization you are trying to delete does not exist';
//       return handleRequestStatus(ctx, 400, message);
//     } else {
//       try {
//         const message = 'The organization deleted successfully';
//         await organizationService.delete(id);
//         return handleRequestStatus(ctx, 200, message);
//       } catch (e) {
//         return handleRequestStatus(ctx, 500, 'Something went wrong');
//       }
//     }
//   }
// }
