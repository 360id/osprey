import { BaseService } from './BaseService';
import { ScanDocument } from '../entity/scan-document';

export class ScanService extends BaseService {
    constructor() {
        super(ScanDocument);
    }
}
