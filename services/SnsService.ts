import { config } from '../config';
import { awsInstance } from '../utils/sns/create-aws-instance';
import { buildAPSPayloadString, buildFCMPayloadString } from '../utils/sns/sns-payload';
import { SnsUser } from '../entity/sns-user';
import { BaseService } from './BaseService';
import { User } from '../entity/user';


export interface PlatformEndpointParams {
    Token: string;
    PlatformApplicationArn: string;
    CustomUserData: string;
}

export interface DeletePlatformEndpointParams {
    EndpointArn: string;
}

export class SnsService extends BaseService {
    constructor() {
        super(SnsUser);
    }
    private static iosARN: string = config.snsPushNotification.snsIosArn;
    private static androidARN: string = config.snsPushNotification.snsAndroidArn;

    static async createPlatformEndpointForDevice(user: User, deviceId, token, platform = 'android')  {

        let applicationArn = '';
        if (platform === 'ios') {
            applicationArn = this.iosARN;
        } else
            if (platform === 'android') {
            applicationArn = this.androidARN;
        }
        const snsParams: PlatformEndpointParams = {
            Token: token,
            PlatformApplicationArn: applicationArn,
            CustomUserData: deviceId
        };

        return new Promise((resolve, reject) => {
            awsInstance().createPlatformEndpoint(snsParams, (err, data) => {
                if (err) {
                    reject(err);
                }
                const snsService = new SnsService();
                const snsUser = new SnsUser();
                snsUser.awsEndpoint = data.EndpointArn;
                snsUser.pushToken = token;
                snsUser.deviceId = deviceId;
                snsUser.userId = user.id;
                snsService.save(snsUser);
                resolve(data.EndpointArn);
            });
        });
    }

    static async deleteEndpoint(snsUser: SnsUser)  {
        const snsParams: DeletePlatformEndpointParams = {
            EndpointArn: snsUser.awsEndpoint,
        };

        return new Promise((resolve, reject) => {
            awsInstance().deleteEndpoint(snsParams, (err, data) => {
                if (err) {
                    reject(err);
                }
                resolve(data);
            });
        });
    }

    static async publishNotification(endpoint, platform = 'android', message) {
            let payloadKey, payload = '';

            if (platform === 'ios') {
                payloadKey = 'APNS';
                payload = buildAPSPayloadString(message);
            } else if (platform === 'android') {
                payloadKey = 'GCM';
                payload = buildFCMPayloadString(message);
            }

            const snsMessage = {};
            snsMessage[payloadKey] = payload;

            const snsParams = {
                Message: JSON.stringify(snsMessage),
                TargetArn: endpoint,
                MessageStructure: 'json'
            };

            return await awsInstance().publish(snsParams);
        }
}
