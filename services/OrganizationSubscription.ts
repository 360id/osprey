import { BaseService } from './BaseService';
import { OrganizationSubscription } from '../entity/organziation-subscribtion';
import { User } from '../entity/user';

export class OrganizationSubscriptionService extends BaseService {
    constructor() {
        super(OrganizationSubscription);
    }

    static async isSubscriptionActive(user: User, id) {
        const organizationSubscriptionService = new OrganizationSubscriptionService();

        const existingOrgSubscription: OrganizationSubscription = await organizationSubscriptionService.getBy({ where: { organizationId: user.organizationId, subscriptionId: id, isActive: true}});

        if (!existingOrgSubscription) {
            return;
        }

        if (existingOrgSubscription) {
            return existingOrgSubscription;
        }
    }
}
