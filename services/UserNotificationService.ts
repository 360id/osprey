import { BaseService } from './BaseService';
import { UserNotification } from '../entity/user-notification';

export class UserNotificationService extends BaseService {
    constructor() {
        super(UserNotification);
    }
}
