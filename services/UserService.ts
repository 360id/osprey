import { BaseService } from './BaseService';
import { User } from '../entity/user';

export class UserService extends BaseService {
  constructor() {
    super(User);
  }
}
