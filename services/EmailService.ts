import { config } from '../config';
import { Mail } from '../components/mail/Mail';
import { User, UserRole } from '../entity/user';
import { getRepository } from 'typeorm';

export interface ToEmail {
  email?: string;
}

export async function sendForgotEmail(forgotToken: string, userEmail: string, name?: string) {
  const mail = new Mail();

  const opt = {
    to: userEmail,
    subject: '360ID Password Recovery',
    template: 'resetPassword',
    context: {
      name: '',
      url: config.resetPasswordUrl + forgotToken,
    },
  };

  if (name) {
    opt.context.name = name;
  }
  mail.send(opt);
}

export async function sendMasterKeyEmail(key: string, userEmail: string, businessName: string, isSuperAdmin?: boolean) {
  const mail = new Mail();
  const alias = isSuperAdmin ? 'admin' : 'individual';
  mail.send({
    to: userEmail,
    subject: 'Register verifier',
    template: 'masterKey',
    context: {
      url: config.env.basePath + `/register-${alias}-verifier/${key}`,
    },
  });
}

export async function sendUserCreateEmail(token: string, userEmail: string) {
  const mail = new Mail();
  mail.send({
    to: userEmail,
    subject: 'Register customer',
    template: 'masterKey',
    context: {
      url: config.env.basePath + `/sign-up/${token}`,
    },
  });
}

export async function sendSupportEmail(emailMessage: string, subject: string, fromEmail: string, toEmail: string) {
  const mail = new Mail();
  mail.send({
    to: toEmail,
    subject: subject,
    template: 'support',
    context: {
      message: emailMessage,
      userEmail: fromEmail,
    },
  });
}

export async function getToEmailAdress(user: User) {
  const toEmail: ToEmail = {};
  if (user.role === UserRole.USER) {
    toEmail.email = 'support@360id.com';
  } else if (user.role === UserRole.INDIVIDUAL_VERIFIER || user.role === UserRole.ADMIN_VERIFIER) {
    if (user.role === UserRole.ADMIN_VERIFIER) {
      toEmail.email = user.email;
    } else {
      const admin: User = await getRepository(User)
        .createQueryBuilder('user')
        .where('user.organizationId = :id AND user.role = :adminRole', {
          id: user.organizationId,
          adminRole: UserRole.ADMIN_VERIFIER,
        })
        .getOne();
      toEmail.email = admin.email;
    }
  }
  return toEmail;
}
