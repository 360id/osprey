import { UserService } from './UserService';
import { User } from '../entity/user';
import { handleRequestStatus } from '../utils/request-status';
import { toUserDto } from '../dto/user-dto';
import { Context } from 'koa';
import * as bcrypt from 'bcrypt';
import { Organization } from '../entity/organization';
import { Status } from '../entity/base-entity';
import { OrganizationService } from './OrganizationService';

export class AuthService {
  static async registerAdminVerifier(ctx: Context) {
    const masterKey = ctx.params.masterKey;
    const businessName = ctx.params.businessName;
    const userService = new UserService();
    const { email, password, data } = ctx.request.body;
    const orgService = new OrganizationService();

    const userOrganization: Organization = await orgService.getBy({ where: { masterKey: masterKey, businessName: businessName } });

    if (!userOrganization) {
      const message = 'Organization dos not exist';
      return handleRequestStatus(ctx, 400, message);
    }

    try {
      const user: User = await userService.getBy({ where: { masterKey: masterKey, email: email } });

      if (!user) {
        const message = 'Wrong email address or master key';
        return handleRequestStatus(ctx, 400, message);
      }
      user.hashedPassword = await bcrypt.hash(password, 10);
      user.data = data;
      user.status = Status.ACTIVE;
      if (userOrganization.status !== Status.ACTIVE) {
        userOrganization.status = Status.ACTIVE;
        await orgService.save(userOrganization);
      }
      await userService.save(user);
      const message = 'Admin user registered successfully';
      return handleRequestStatus(ctx, 200, message, toUserDto(user));
    } catch (e) {
      return handleRequestStatus(ctx, 500);
    }
  }

  static async registerIndividualVerifier(ctx: Context) {
    const individualKey = ctx.params.individualKey;

    const userService = new UserService();
    const { email, password, data } = ctx.request.body;

    const user: User = await userService.getBy({ where: { email: email, individualKey: individualKey } });
    if (!user) {
      const message = 'Wrong email address or individual key';
      return handleRequestStatus(ctx, 400, message);
    }

    try {
      user.hashedPassword = await bcrypt.hash(password, 10);
      user.data = data;
      user.status = Status.ACTIVE;
      await userService.save(user);
      const message = 'Individual user registered successfully';
      return handleRequestStatus(ctx, 200, message, toUserDto(user));
    } catch (e) {
      return handleRequestStatus(ctx, 500);
    }
  }
}
