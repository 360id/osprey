import { BaseService } from './BaseService';
import { Subscription } from '../entity/subscription';
import { User } from '../entity/user';
import { OrganizationService } from './OrganizationService';
import { StripeComponent } from '../components/stripe';
import { getRepository } from 'typeorm';
import { ScanDocument } from '../entity/scan-document';
import { Organization } from '../entity/organization';
import dayjs from 'dayjs';

export class SubscriptionService extends BaseService {
    constructor() {
        super(Subscription);
    }

    static async getFacialRecordsCount(orgId: number) {
        const currentDate = dayjs(new Date(), { utc: true }).format();
        const firstDay = dayjs().startOf('month').format();

        return await getRepository(ScanDocument)
            .createQueryBuilder('facialRecord')
            .select('facialRecord.organizationId', 'organizationId')
            .addSelect('COUNT(facialRecord.id)', 'recordsCount')
            .where('facialRecord.scanDate >= :fromDate' )
            .andWhere('facialRecord.scanDate <= :toDate')
            .groupBy('facialRecord.organizationId')
            .setParameters({'facialRecord.organizationId': orgId, fromDate: firstDay, toDate: currentDate})
            .getRawOne();
    }

    static async updateSubscriptionPrice(user: User) {
        const orgId = user.organizationId;
        const recordCount = await this.getFacialRecordsCount(orgId);
        const organizationService = new OrganizationService();
        const organization: Organization = await organizationService.getOne(orgId);

        const stripeSubscription = await StripeComponent.retrieveSubscription(organization.stripeSubscriptionId);

        const subscriptionItemId = stripeSubscription.items.data[0].id;
        const timestamp = Math.floor(Date.now() / 1000);

        return await StripeComponent.createUsageRecord(subscriptionItemId, {
                quantity: recordCount.recordsCount,
                timestamp: timestamp
        });
    }

    static async canceleSubscription(user: User) {
        const orgId = user.organizationId;
        const organizationService = new OrganizationService();
        const organization: Organization = await organizationService.getOne(orgId);

       return await StripeComponent.cancelSubscription(organization.stripeSubscriptionId);
    }
}
