import axios from 'axios';
import { config } from '../config';

export interface DriverLicense {
  BirthDate: string;
  GivenName: string;
  MiddleName?: string;
  FamilyName: string;
  LicenceNumber: string;
  StateOfIssue: string;
  ExpiryDate?: string;
}

export interface Passport {
  BirthDate: string;
  GivenName: string;
  MiddleName: string;
  FamilyName: string;
  TravelDocumentNumber: string;
  Gender: string;
  ExpiryDate?: string;
}

export default class RapidIdService {
  token: string = config.rapidId.authToken;
  baseUrl: string = config.rapidId.apiUrl;

  public async verifyDriversLicence(data: DriverLicense) {
    const validationResponse = await axios({
      baseURL: this.baseUrl,
      method: 'post',
      url: '/dvs/driverLicence',
      data,
      headers: { token: this.token, 'Content-Type': 'application/json' }
    });

    const success =
      validationResponse &&
      validationResponse['VerifyDocumentResult'] &&
      validationResponse['VerifyDocumentResult']['VerificationResultCode'] === 'Y';

    return { ...validationResponse, success };
  }

  public async verifyPassport(data: Passport) {
    const validationResponse = await axios({
      baseURL: this.baseUrl,
      method: 'post',
      url: '/dvs/passport',
      data,
      headers: { token: this.token, 'Content-Type': 'application/json' }
    });

    const success =
      validationResponse &&
      validationResponse['VerifyDocumentResult'] &&
      validationResponse['VerifyDocumentResult']['VerificationResultCode'] === 'Y';

    return { ...validationResponse, success };
  }
}
