import { BaseService } from './BaseService';
import { Notification } from '../entity/notification';

export class NotificationService extends BaseService {
    constructor() {
        super(Notification);
    }
}
