import { BaseService } from './BaseService';
import { User } from '../entity/user';

export class UserService extends BaseService {
  static getBy: any;
  constructor() {
    super(User);
  }
}
