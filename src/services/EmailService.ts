import { config } from '../config';
import { Mail } from '../components/mail/Mail';
import { User, UserRole } from '../entity/user';
import { getRepository } from 'typeorm';

export interface ToEmail {
  email?: string;
}

async function sendForgotEmail(forgotToken: string, userEmail: string, name?: string) {
  const mail = new Mail();

  const opt = {
    to: userEmail,
    subject: '360ID Password Recovery',
    template: 'resetPassword',
    context: {
      name: name || '',
      url: `${config.resetPasswordUrl}/${forgotToken}`,
    },
  };

  mail.send(opt);
}

async function sendMasterKeyEmail(key: string, userEmail: string, businessName: string, isSuperAdmin?: boolean) {
  const mail = new Mail();
  const alias = isSuperAdmin ? 'admin' : 'individual';
  mail.send({
    to: userEmail,
    subject: 'Register verifier',
    template: 'new-organization',
    context: {
      url: config.env.basePath + `/register-${alias}-verifier/${key}`,
      businessName,
    },
  });
}

async function sendUserCreateEmail(key: string, userEmail: string) {
  const mail = new Mail();
  mail.send({
    to: userEmail,
    subject: 'Register customer',
    template: 'masterKey',
    context: {
      url: config.env.basePath + `/sign-up/${key}`,
    },
  });
}

async function sendSupportEmail(emailMessage: string, subject: string, fromEmail: string, toEmail: string) {
  const mail = new Mail();
  mail.send({
    to: toEmail,
    subject: subject,
    template: 'support',
    context: {
      message: emailMessage,
      userEmail: fromEmail,
    },
  });
}

async function getToEmailAdress(user: User) {
  const toEmail: ToEmail = {};
  if (user.role === UserRole.USER) {
    toEmail.email = 'support@360id.com';
  } else if (user.role === UserRole.INDIVIDUAL_VERIFIER || user.role === UserRole.ADMIN_VERIFIER) {
    if (user.role === UserRole.ADMIN_VERIFIER) {
      toEmail.email = user.email;
    } else {
      const admin: User = await getRepository(User)
        .createQueryBuilder('user')
        .where('user.organizationId = :id AND user.role = :adminRole', {
          id: user.organizationId,
          adminRole: UserRole.ADMIN_VERIFIER,
        })
        .getOne();
      toEmail.email = admin.email;
    }
  }
  return toEmail;
}

export interface EmailService {
  getToEmailAdress: (user: User) => Promise<ToEmail>;
  sendSupportEmail: (emailMessage: string, subject: string, fromEmail: string, toEmail: string) => Promise<void>;
  sendUserCreateEmail: (token: string, userEmail: string) => Promise<void>;
  sendMasterKeyEmail: (key: string, userEmail: string, businessName: string, isSuperAdmin?: boolean) => Promise<void>;
  sendForgotEmail: (forgotToken: string, userEmail: string, name?: string) => Promise<void>;
}

export { getToEmailAdress, sendSupportEmail, sendUserCreateEmail, sendMasterKeyEmail, sendForgotEmail };
