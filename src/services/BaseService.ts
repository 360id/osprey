import { getRepository, Repository } from 'typeorm';
import _ from 'lodash';

interface IBaseService {
  repository: Repository<any>;
  Model: any;

  getBy: (params: any) => Promise<any>;
}

interface Create<T> {}
export class BaseService implements IBaseService {
  public repository: Repository<any>;
  public Model;

  constructor(Model) {
    this.repository = getRepository(Model);
  }

  async getOne(id) {
    return await this.repository.findOne(id);
  }

  async getBy(params) {
    const model = await this.repository.find(params);

    if (!model || _.isEmpty(model[0])) {
      return;
    }
    return model[0];
  }

  async create(data) {
    return await this.repository.create(data);
  }

  async save(data) {
    return await this.repository.save(data);
  }

  async update(params, data) {
    return await this.repository.update(params, data);
  }

  async delete(params: number | Array<number> | object | string) {
    return await this.update(params, { deleted: true });
  }

  async getList({ skip, take, deleted }) {
    const params: any = {
      where: [{ deleted: deleted }],

      order: { id: 'DESC' },
      take: take,
    };
    if (skip) {
      params.skip = skip;
    }
    const list = await this.repository.find(params);
    if (!list) {
      return;
    }
    return list;
  }
}
