import { BaseService } from './BaseService';
import { Organization } from '../entity/organization';

export class OrganizationService extends BaseService {
  constructor() {
    super(Organization);
  }
}
