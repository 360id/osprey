import passport from 'passport';
const LocalStrategy = require('passport-local').Strategy;
import passportJWT from 'passport-jwt';
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const authConfig = require('./config').config;
import { UserService } from './services/UserService';
import { GraphQLLocalStrategy } from 'graphql-passport';
import { User } from './entity/user';

export function passportConfig() {
  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      async (username, password, done) => {
        try {
          const userService = new UserService();
          const user = await userService.getOne({ email: username });
          if (!user) {
            return done(undefined, false, { message: 'Incorrect email.' });
          }

          const passwordMatch = await user.comparePassword(password);
          if (!passwordMatch) {
            return done(undefined, false, { message: 'Incorrect password.' });
          }
          if (user) {
            return done(undefined, user);
          }
        } catch (e) {
          return done(e);
        }
      }
    )
  );

  passport.use(
    new GraphQLLocalStrategy(async (email, password, done) => {
      try {
        const userService = new UserService();
        const user = await userService.getOne({ email: email });

        if (!user) {
          return done(new Error('Incorrect email'), null);
        }

        const passwordMatch = await user.comparePassword(password);
        if (!passwordMatch) {
          return done(new Error('Incorrect password'), null);
        }
        if (user) {
          return done(undefined, user);
        }
      } catch (e) {
        return done(e);
      }
    })
  );

  passport.serializeUser((user: User, done) => {
    done(undefined, user.id);
  });

  passport.deserializeUser((id, done) => {
    const userService = new UserService();
    return userService
      .getOne(id)
      .then(user => {
        done(undefined, user);
      })
      .catch(err => {
        done(err, undefined);
      });
  });

  passport.use(
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: authConfig.jwtSecret,
      },
      function (jwtPayload, cb) {
        const userService = new UserService();
        // find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
        return userService
          .getOne(jwtPayload.id)
          .then(user => {
            return cb(undefined, user);
          })
          .catch(err => {
            return cb(err);
          });
      }
    )
  );
}
