import Joi from 'joi';

const authValidateSchemas = {
  register: Joi.object().keys({
    email: Joi.string()
      .email()
      .required()
      .error(() => 'Email is empty or not valid'),
    password: Joi.string()
      .required()
      .error(() => 'Password is required'),
    confirmPassword: Joi.string()
      .valid(Joi.ref('password'))
      .error(() => 'Passwords do not match or empty'),
    data: Joi.object().keys({
      firstName: Joi.string()
        .required()
        .error(() => 'Name name is required'),
      lastName: Joi.string()
        .required()
        .error(() => 'Surname name is required'),
      phoneNumber: Joi.number().error(() => 'Phone number name is required'),
      mobileProvider: Joi.string().error(() => 'Phone number name is required')
    })
  }),
  forgot: Joi.object().keys({
    email: Joi.string()
      .email()
      .required()
      .error(() => 'Email is empty or not valid')
  }),
  reset: Joi.object().keys({
    password: Joi.string()
      .required()
      .error(() => 'Password is required'),
    confirmPassword: Joi.string()
      .valid(Joi.ref('password'))
      .error(() => 'Passwords do not match or empty')
  }),
  registerAdminVerifier: Joi.object().keys({
    email: Joi.string()
      .email()
      .required()
      .error(() => 'Email is empty or not valid'),
    password: Joi.string()
      .required()
      .error(() => 'Password is required'),
    confirmPassword: Joi.string()
      .valid(Joi.ref('password'))
      .error(() => 'Passwords do not match or empty'),
    data: Joi.object().keys({
      firstName: Joi.string()
        .required()
        .error(() => 'Name name is required'),
      lastName: Joi.string()
        .required()
        .error(() => 'Surname name is required'),
      phoneNumber: Joi.number()
        .required()
        .error(() => 'Phone number name is required'),
      mobileProvider: Joi.string()
        .required()
        .error(() => 'Phone number name is required')
    })
  }),
  registerIndividualVerifier: Joi.object().keys({
    email: Joi.string()
      .email()
      .required()
      .error(() => 'Email is empty or not valid'),
    password: Joi.string()
      .required()
      .error(() => 'Password is required'),
    confirmPassword: Joi.string()
      .valid(Joi.ref('password'))
      .error(() => 'Passwords do not match or empty'),
    data: Joi.object().keys({
      firstName: Joi.string()
        .required()
        .error(() => 'Name name is required'),
      lastName: Joi.string()
        .required()
        .error(() => 'Surname name is required'),
      phoneNumber: Joi.number()
        .required()
        .error(() => 'Phone number name is required'),
      mobileProvider: Joi.string()
        .required()
        .error(() => 'Phone number name is required')
    })
  })
};

export { authValidateSchemas };
