import Joi from 'joi';

const userValidateSchema = {
  createUser: Joi.object().keys({
    email: Joi.string()
      .email()
      .required()
      .error(() => 'Email is empty or not valid'),
    data: Joi.object().optional()
  }),
  updateUser: Joi.object().keys({
    email: Joi.string()
      .email()
      .error(() => 'Email is not valid'),
    data: Joi.object().optional(),
  }),
  updateSettings: Joi.object().keys({
    settings: Joi.object().keys({
        isRecordPublic: Joi.boolean(),
    }),
  }),
  support: Joi.object().keys({
      userEmail: Joi.string()
          .email()
          .required()
          .error(() => 'Email is empty or not valid'),
      subject: Joi.string()
          .allow(''),
      emailMessage: Joi.string()
          .required()
          .error(() => 'Email message si empty'),
  }),
};

export { userValidateSchema };
