import Joi from 'joi';
import { AudienceTypes, RequestsFrequency } from '../entity/notification';


const notificationValidateSchema = {
    createNotification: Joi.object().keys({
        messageText: Joi.string().required().error(() => 'MessageText is required'),
        startDate: Joi.date().required().error(() => 'Start date is required'),
        hasUntilDate: Joi.boolean().optional(),
        untilDate: Joi.date().allow(''),
        audienceType: Joi.string().valid(AudienceTypes.ALL, AudienceTypes.ADMIN_VERIFIER, AudienceTypes.INDIVIDUAL_VERIFIER, AudienceTypes.USER).error(() => 'Audience type must be one of all_users, admin_verifiers, individual_verifiers or users'),
        requestsFrequency: Joi.string().valid(RequestsFrequency.RECURRING, RequestsFrequency.DAILY, RequestsFrequency.WEEKLY, RequestsFrequency.YEARLY, RequestsFrequency.ONETIME).error(() => 'Requests frequency type must be one of onetime, recurring, yearly, monthly, weekly or daily'),
    }),
    updateNotification: Joi.object().keys({
        messageText: Joi.string().optional(),
        startDate: Joi.date().allow(''),
        hasUntilDate: Joi.boolean().optional(),
        untilDate: Joi.date().allow(''),
        audienceType: Joi.string().valid(AudienceTypes.ALL, AudienceTypes.ADMIN_VERIFIER, AudienceTypes.INDIVIDUAL_VERIFIER, AudienceTypes.USER).error(() => 'Audience type must be one of all_users, admin_verifiers, individual_verifiers or users'),
        requestsFrequency: Joi.string().valid(RequestsFrequency.RECURRING, RequestsFrequency.MONTHLY, RequestsFrequency.DAILY, RequestsFrequency.WEEKLY, RequestsFrequency.YEARLY, RequestsFrequency.ONETIME).error(() => 'Requests frequency type must be one of onetime, recurring yearly, monthly, weekly or daily'),
    }),
};

export { notificationValidateSchema };
