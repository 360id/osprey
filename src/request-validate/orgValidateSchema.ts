import Joi from 'joi';
import { Status } from '../entity/base-entity';

const orgValidateSchema = {
  createOrg: Joi.object().keys({
    businessName: Joi.string()
      .required()
      .error(() => 'Business name is required'),
    data: Joi.object().keys({
      email: Joi.string()
        .email()
        .required()
        .error(() => 'Email is empty or not valid'),
      businessAddress: Joi.object().keys({
        addressLine1: Joi.string()
          .required()
          .error(() => 'Business address is required'),
        addressLine2: Joi.string().optional()
      }),
      city: Joi.string()
        .required()
        .error(() => 'City is required'),
      state: Joi.string()
        .required()
        .error(() => 'State is required'),
      zip: Joi.string()
        .required()
        .error(() => 'Zip code is required'),
      firstName: Joi.string()
        .required()
        .error(() => 'First name is required'),
      lastName: Joi.string()
        .required()
        .error(() => 'Last name is required')
    }),
    masterKey: Joi.string()
      .required()
      .error(() => 'Master key is required'),
    status: Joi.string()
      .valid(Status.INACTIVE, Status.DRAFT, Status.ACTIVE)
      .error(() => 'Status must be one of inactive, draft')
  }),
  updateOrg: Joi.object().keys({
    status: Joi.string()
      .valid(Status.INACTIVE, Status.DRAFT, Status.ACTIVE)
      .error(() => 'Status must be one of inactive, draft'),
    data: Joi.object().optional(),
    businessName: Joi.string().allow('')
  })
};

export { orgValidateSchema };
