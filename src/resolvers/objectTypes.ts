import { ObjectType, Field } from 'type-graphql';
import { User, Organization } from '../entity';

@ObjectType()
export class UserResponse {
  @Field()
  user: User;
}

@ObjectType()
export class SuccessFailureResponse {
  @Field()
  ok: boolean;

  @Field()
  message?: string;
}

@ObjectType()
export class OrgUsers {
  @Field(type => [User])
  users: User[];

  @Field()
  totalMembers: number;

  @Field()
  individualVerifierCount: number;

  @Field()
  adminVerifierCount: number;
}

@ObjectType()
class FieldError {
  @Field()
  field: string;

  @Field()
  message: string;
}

@ObjectType()
export class OrganizationResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => Organization, { nullable: true })
  organization?: Organization;
}

@ObjectType()
export class UserDto {
  @Field()
  id: string;

  @Field()
  email: string;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;

  @Field()
  role: string;

  @Field()
  status: string;

  @Field({ nullable: true })
  organizationId?: string | undefined;

  @Field({ nullable: true })
  individualKey?: string | undefined;

  @Field({ nullable: true })
  isOnboarding?: boolean | undefined;

  @Field({ nullable: true })
  firstName?: string | undefined;

  @Field({ nullable: true })
  lastName?: string | undefined;

  @Field({ nullable: true })
  phoneNumber?: string | undefined;
}
@ObjectType()
export class OrgUsersData {
  @Field(type => [UserDto])
  users: UserDto[];

  @Field()
  totalMembers: number;

  @Field()
  individualVerifierCount: number;

  @Field()
  adminVerifierCount: number;
}

@ObjectType()
export class GenericResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => Organization, { nullable: true })
  organization?: Organization;

  @Field(() => UserDto, { nullable: true })
  customer?: UserDto;
}
