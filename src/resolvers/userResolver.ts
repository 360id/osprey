import { Resolver, Query, Arg, Mutation, Ctx } from 'type-graphql';
import { User, UserRole } from '../entity/user';
import { UserService } from '../services/UserService';
import { toUserDto } from '../dto/user-dto';
import { CreateUserInput, UpdateUserInput } from './inputTypes';
import { IContext } from '../types/IContext';
import { generateToken } from '../utils/user-utils';
import { UserDto } from './objectTypes';

@Resolver()
export class UserResolver {
  @Query(() => UserDto)
  async getUser(@Arg('id') id: string): Promise<UserDto> {
    const userService = new UserService();
    try {
      const user: User = await userService.getOne(id || 0);

      if (!user) {
        const message = 'The user you are trying to get does not exist';
        throw new Error(message);
      }
      return toUserDto(user);
    } catch (e) {
      throw new Error('user not found');
    }
  }

  /**
   * Creates user belonging to the organization
   */

  @Mutation(() => User)
  async createUser(@Arg('input') input: CreateUserInput, @Ctx() context: IContext) {
    const { email, organizationId } = input;
    const {
      mailer: { sendMasterKeyEmail },
    } = context;
    const userService = new UserService();
    const user = await userService.getBy({ email: email });

    if (user) {
      const message = 'User email already exist';
      throw new Error(message);
    }

    const userToBeSaved: User = new User();
    userToBeSaved.email = email;
    userToBeSaved.role = UserRole.INDIVIDUAL_VERIFIER;
    userToBeSaved.organizationId = organizationId;

    try {
      const user = await userService.save(userToBeSaved);
      console.log('User', user);
      const token = generateToken(toUserDto(user), '1h');
      await sendMasterKeyEmail(token, email, '', false);
      return user;
    } catch (e) {
      throw new Error('Something bad happened');
    }
  }

  /**
   * Updates a user
   */
  @Mutation(() => User)
  async updateUser(@Arg('id') id: string, @Arg('input') input: UpdateUserInput) {
    const userService = new UserService();

    const user: User = await userService.getOne(id);

    if (!user) {
      const message = 'The user you are trying to update does not exist';
    }

    try {
      await userService.update(id, input);
      const message = 'The user updated successfully';
      return user;
    } catch (e) {
      console.log('what');
    }
  }

  @Mutation(() => Boolean)
  async deleteUser(@Arg('id') id: string) {
    const userService = new UserService();
    const userToRemove: User = await userService.getOne(id);
    if (!userToRemove) {
      const message = 'The user you are trying to delete does not exist';
      console.log(message);
    } else {
      try {
        const message = 'The user deleted successfully';
        await userService.delete(id);
        return true;
      } catch (e) {
        console.log('I am errored');
      }
    }
  }
}
