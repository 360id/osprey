import { Resolver, Query, Arg, Mutation, InputType, Field, Ctx, ObjectType } from 'type-graphql';
import crypto from 'crypto';
import { User, Organization } from '../entity';
import { UserService, OrganizationService } from '../services';
import { Status } from '../entity/base-entity';
import { toUserDto } from '../dto/user-dto';
import { generateToken, verifyToken } from '../utils/user-utils';
import { UserDto } from './objectTypes';
import { MoreThan } from 'typeorm';
import * as bcrypt from 'bcrypt';
import * as argon2 from 'argon2';
import {
  UserInput,
  AuthInput,
  ForgotPasswordInput,
  ResetPasswordInput,
  RegisterVerifierInput,
  IndividualVerifierInput,
  RegisterAdminInput,
} from './inputTypes';
import { SuccessFailureResponse, UserResponse } from './objectTypes';
import { IContext } from '../types/IContext';
import { successResponse } from '../utils/request-status';

export async function generateForgotToken() {
  return crypto.randomBytes(20).toString('hex');
}

@Resolver()
export class AuthResolver {
  /**
   * Get current user
   */
  @Query(() => User, { nullable: true })
  async currentUser(@Ctx() context: IContext): Promise<User | null> {
    const user = context.getUser();
    if (!user) return null;
    return user;
  }

  /**
   * Register user
   */
  @Mutation(() => UserResponse)
  async register(@Arg('input') input: UserInput, @Ctx() context: IContext): Promise<UserResponse> {
    const userService = new UserService();
    const userToBeSaved: User = new User();
    const { email, password, firstName, lastName, phoneNumber } = input;

    userToBeSaved.password = password;
    userToBeSaved.email = email;
    userToBeSaved.firstName = firstName;
    userToBeSaved.lastName = lastName;
    userToBeSaved.phoneNumber = phoneNumber;

    const user = await userService.getBy({ email: userToBeSaved.email });

    if (user) {
      const message = 'User already exist';
      throw new Error(message);
    }

    try {
      const user = await userService.create(userToBeSaved);
      const savedUser = await userService.save(user);

      //Login after successfully registering
      await context.login(savedUser);

      return { user: savedUser };
    } catch (e) {
      throw new Error('Failed to register user');
    }
  }

  /**
   * Login user
   */
  @Mutation(() => UserResponse)
  async login(@Arg('input') input: AuthInput, @Ctx() context: IContext) {
    const { email, password } = input;
    try {
      const { user } = await context.authenticate('graphql-local', { email, password });
      await context.login(user);
      return { user };
    } catch (e) {
      throw new Error(e);
    }
  }

  /**
   * Logout user
   */
  @Mutation(() => SuccessFailureResponse)
  async logout(@Ctx() context: IContext) {
    if (context.isAuthenticated()) {
      context.logout();
      return successResponse();
    }
    throw new Error('Failed to logout user');
  }

  /**
   * Handle forgot password flow
   */
  @Mutation(() => SuccessFailureResponse)
  async forgotPassword(@Arg('input') input: ForgotPasswordInput, @Ctx() context: IContext) {
    const { email } = input;
    const { mailer } = context;
    const userService = new UserService();

    const user = await userService.getBy({ email: email });

    if (!user) {
      throw new Error('The user you are trying to get does not exist');
    }

    user.resetPasswordToken = await generateForgotToken();
    user.resetPasswordExpire = Date.now() + 3600000;

    try {
      await userService.save(user);

      // Send email
      const { firstName, resetPasswordToken, email } = user;
      await mailer.sendForgotEmail(resetPasswordToken, email, firstName);
      return successResponse('Email sent');
    } catch (e) {
      throw new Error(e);
    }
  }

  @Mutation(() => SuccessFailureResponse)
  async resetPassword(@Arg('input') input: ResetPasswordInput) {
    const { token, password } = input;
    const userService = new UserService();

    const user = await userService.getBy({
      resetPasswordToken: token,
      resetPasswordExpire: MoreThan(Date.now()),
    });

    if (!user) {
      throw new Error('Password reset token is invalid or has expired.');
    }

    user.password = await argon2.hash(password);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;

    await userService.save(user);

    return successResponse('Password reset successfully');
  }

  @Mutation(() => SuccessFailureResponse)
  async registerAdminVerifier(@Arg('input') input: RegisterVerifierInput) {
    const { masterKey, businessName, email, password } = input;

    const userService = new UserService();
    const orgService = new OrganizationService();

    const userOrganization: Organization = await orgService.getBy({
      where: { masterKey: masterKey, businessName: businessName },
    });

    if (!userOrganization) {
      const message = 'Organization dos not exist';
      throw new Error(message);
    }

    try {
      const user: User = await userService.getBy({ where: { masterKey: masterKey, email: email } });

      if (!user) {
        const message = 'Wrong email address or master key';
        throw new Error(message);
      }
      user.password = await bcrypt.hash(password, 10);
      user.status = Status.ACTIVE;
      if (userOrganization.status !== Status.ACTIVE) {
        userOrganization.status = Status.ACTIVE;
        await orgService.save(userOrganization);
      }
      await userService.save(user);
      const message = 'Admin user registered successfully';
      return successResponse(message);
    } catch (e) {
      throw new Error('Registration failed.');
    }
  }

  @Mutation(() => SuccessFailureResponse)
  async registerIndividualVerifier(@Arg('input') input: IndividualVerifierInput, @Ctx() context) {
    const { individualKey, businessName, email, password, data } = input;
    const { UserService } = context;

    const userService = new UserService();

    const user: User = await userService.getBy({ where: { email: email, individualKey: individualKey } });

    if (!user) {
      const message = 'Wrong email address or individual key';
      throw new Error(message);
    }

    try {
      user.password = await bcrypt.hash(password, 10);
      user.status = Status.ACTIVE;
      await userService.save(user);
      const message = 'Individual user registered successfully';
      return successResponse(message);
    } catch (e) {
      throw new Error('Registration failed');
    }
  }

  /**
   * Register Admin
   */
  @Mutation(() => SuccessFailureResponse)
  async registerAdmin(@Arg('input') input: RegisterAdminInput) {
    const { firstName, lastName, password, token } = input;

    const userService = new UserService();
    const orgService = new OrganizationService();

    const verifiedToken = verifyToken(token) as UserDto;
    const organizationId = typeof verifiedToken === 'object' ? verifiedToken.organizationId : '';

    async function activateOrganization(organizationId: string) {
      const userOrganization: Organization = await orgService.getBy({
        where: { id: organizationId },
      });

      if (!userOrganization) {
        const message = 'Organization dos not exist';
        throw new Error(message);
      }

      if (userOrganization.status !== Status.ACTIVE) {
        userOrganization.status = Status.ACTIVE;
        await orgService.save(userOrganization);
      }
    }

    if (organizationId) {
      await activateOrganization(organizationId);
    }

    try {
      const user = await userService.getBy({ where: { email: verifiedToken.email } });

      if (!user) {
        const message = 'Wrong email address';
        throw new Error(message);
      }

      user.password = await argon2.hash(password);
      user.status = Status.ACTIVE;
      user.firstName = firstName;
      user.lastName = lastName;

      await userService.save(user);
      const message = 'Admin user registered successfully';
      return successResponse(message);
    } catch (e) {
      throw new Error('Registration failed.');
    }
  }
}
