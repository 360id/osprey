import { Resolver, Query, Args, Arg, Mutation, ArgsType, Field, Int, Ctx, ObjectType, Authorized } from 'type-graphql';
import { Min, Max } from 'class-validator';
import { UserInputError, ApolloError } from 'apollo-server';

import { User, UserRole } from '../entity/user';
import { UserService, OrganizationService } from '../services';
import { toUserDto } from '../dto/user-dto';
import { successResponse } from '../utils/request-status';
import { SuccessFailureResponse, OrganizationResponse, UserDto, OrgUsersData } from './objectTypes';
import { CreateOrganizationInput, UpdateOrganizationInput } from './inputTypes';
import { Organization } from '../entity';
import { Status } from '../entity/base-entity';
import { toOrganizationDto } from '../dto/organization-dto';
import { Brackets, Equal, getRepository, Not } from 'typeorm';
import { omitBy, isNil } from 'lodash';
import { IContext } from '../types/IContext';
import { generateToken } from '../utils/user-utils';

@ArgsType()
class OrganizationsArgs {
  @Field(type => Int)
  @Min(0)
  skip: number = 0;

  @Field(type => Int)
  @Min(1)
  @Max(50)
  take: number = 25;

  @Field({ nullable: true })
  businessName?: string;
}

@ArgsType()
class OrgUsersArg {
  @Field()
  id: string;

  @Field({ nullable: true })
  name?: string;
}

@Resolver()
export class OrganizationResolver {
  /**
   * Get all organizations
   */
  @Query(returns => [Organization])
  async getOrganizations(@Args() { skip, take, businessName }: OrganizationsArgs) {
    try {
      const query = getRepository(Organization)
        .createQueryBuilder('organization')
        .leftJoinAndSelect('organization.users', 'user', 'user.role = :role')
        .setParameter('role', UserRole.ADMIN_VERIFIER)
        .where('organization.deleted = :deleted', { deleted: false });

      if (businessName) {
        query.andWhere('organization.businessName ILIKE :businessName', { businessName: '%' + businessName + '%' });
      }

      query.addOrderBy('organization.createdAt', 'DESC');
      query.take(take);
      query.skip(skip);

      const orgs: Organization[] = await query.getMany();

      const parsedOrgs = orgs.map(organization => {
        return toOrganizationDto(organization);
      });

      return orgs;
    } catch (e) {
      throw Error('failed');
    }
  }

  /**
   * Get an organization
   */
  @Query(returns => Organization)
  async getOrganization(@Arg('id') id: string) {
    const orgId = id;
    try {
      const organizationService = new OrganizationService();
      const organization = await organizationService.getOne(orgId);

      const toDtoOrg = toOrganizationDto(organization);

      if (!organization) {
        const message = 'The organization you are trying to get not exist';
        throw new UserInputError(message);
      }
      return toDtoOrg;
    } catch (e) {
      throw new ApolloError(e);
    }
  }

  /**
   * Create an Organization
   */
  @Authorized(['super_admin'])
  @Mutation(() => OrganizationResponse)
  async createOrganization(
    @Arg('input') input: CreateOrganizationInput,
    @Ctx() context: IContext
  ): Promise<OrganizationResponse> {
    const { mailer } = context;

    const { sendMasterKeyEmail } = mailer;
    const adminOrg: User = new User();
    const userService = new UserService();
    const organizationService = new OrganizationService();
    const {
      businessName,
      status,
      masterKey,
      owner: { firstName, lastName, email },
      businessAddress,
    } = input;

    const user = await userService.getBy({ email });

    if (user) {
      return {
        errors: [{ field: 'email', message: 'User already exists' }],
      };
    }

    try {
      const isExistOrg = await organizationService.getBy({
        where: [{ businessName: businessName }],
      });

      console.log('isExistOrg', isExistOrg);

      if (isExistOrg) {
        return {
          errors: [{ field: 'businessName', message: 'Organization name or master key already taken' }],
        };
      }

      adminOrg.firstName = firstName;
      adminOrg.lastName = lastName;
      adminOrg.email = email;
      adminOrg.role = UserRole.ADMIN_VERIFIER;
      adminOrg.status = status;
      adminOrg.masterKey = masterKey;

      const createdUser = await userService.save(adminOrg);

      let organizationToBeSaved: Organization = new Organization();
      organizationToBeSaved = {
        ...organizationToBeSaved,
        masterKey,
        status,
        businessName,
        owner: createdUser,
        businessAddress,
      };
      const createdOrganization = await organizationService.save(organizationToBeSaved);
      createdUser.organizationId = createdOrganization.id;
      const savedUser = await userService.save(createdUser);
      console.log('SavedUser', toUserDto(savedUser));

      const token = generateToken(toUserDto(savedUser), '1h');
      console.log('Token', token);
      if (status === Status.INACTIVE) {
        try {
          console.log('sending email');
          await sendMasterKeyEmail(token, createdUser.email, createdOrganization.businessName, true);
        } catch (e) {
          throw new Error('Mail was not send');
        }
      }

      return {
        organization: createdOrganization,
      };
    } catch (e) {
      throw new Error(e);
    }
  }

  /**
   * Update Organization
   */
  @Mutation(() => SuccessFailureResponse)
  async updateOrganization(@Arg('id') id: string, @Arg('input') input: UpdateOrganizationInput) {
    const organizationService = new OrganizationService();

    const organization: Organization = await organizationService.getOne(id || 0);
    if (!organization) {
      const message = 'The organization you are trying to update  not exist';
      throw new UserInputError(message);
    }

    const {
      businessName,
      status,
      masterKey,
      owner: { firstName, lastName, email },
      businessAddress,
    } = input;

    const isExistOrg = await organizationService.getBy({ businessName: businessName, id: Not(Equal(id)) });

    if (isExistOrg) {
      const message = 'Organization name already taken';
      throw new UserInputError(message);
    }

    const organizationToBeUpdate = omitBy(
      {
        status: status,
        businessName,
      },
      isNil
    );

    try {
      await organizationService.update(id, organizationToBeUpdate);
      const message = 'The organization updated successfully';
      return successResponse(message);
    } catch (e) {
      throw new Error(e);
    }
  }

  /**
   * Delete an ogranization
   */
  @Mutation(() => SuccessFailureResponse)
  async deleteOrganization(@Arg('id') id: string) {
    const organizationService = new OrganizationService();
    const organizationToRemove: User = await organizationService.getOne(id || 0);
    if (!organizationToRemove) {
      const message = 'The organization you are trying to delete does not exist';
      throw new UserInputError(message);
    } else {
      try {
        const message = 'The organization deleted successfully';
        await organizationService.delete(id);
        return successResponse(message);
      } catch (e) {
        throw new Error(e);
      }
    }
  }

  /**
   * Get users belonging to organization
   */
  @Query(() => OrgUsersData)
  async getOrganizationUsers(@Args() { id, name }: OrgUsersArg): Promise<OrgUsersData> {
    const orgId = id;

    try {
      const query = getRepository(User).createQueryBuilder('user').where('user.organizationId = :id', { id: orgId });
      if (name) {
        query.andWhere(
          new Brackets(qb => {
            qb.where("data->>'firstName' ILIKE :data", { data: '%' + name + '%' }).orWhere(
              "data->>'lastName' ILIKE :data",
              {
                data: '%' + name + '%',
              }
            );
          })
        );
      }

      query.andWhere('user.deleted = :deleted', {
        deleted: false,
      });

      const users = await query.getMany();
      if (!users) {
        const message = 'The organization does not have users';
        throw new UserInputError(message);
      }
      const parsedUsers = users.map(user => {
        return toUserDto(user);
      });

      const individualVerifiers = parsedUsers.filter(user => user.role === UserRole.INDIVIDUAL_VERIFIER);
      const adminVerifier = parsedUsers.filter(user => user.role === UserRole.ADMIN_VERIFIER);

      const orgUsersData = {
        users: parsedUsers,
        totalMembers: parsedUsers.length,
        individualVerifierCount: individualVerifiers.length,
        adminVerifierCount: adminVerifier.length,
      };
      return orgUsersData;
    } catch (e) {
      throw new Error(e);
    }
  }
}
