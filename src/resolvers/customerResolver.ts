import { Resolver, Query, Args, Arg, Mutation, ArgsType, Field, Int, Ctx } from 'type-graphql';
import { Brackets, getRepository } from 'typeorm';
import { Min, Max } from 'class-validator';

import { User, UserRole } from '../entity/user';
import { UserService } from '../services/UserService';
import { toUserDto } from '../dto/user-dto';
import { successResponse } from '../utils/request-status';
import { SuccessFailureResponse, UserDto, GenericResponse } from './objectTypes';
import { UpdateCustomerInput, CustomerSettingsInput, CreateCustomerInput, CustomerSupportInput } from './inputTypes';
import { IContext } from '../types/IContext';
import { generateToken } from '../utils/user-utils';
import { ConsoleTransportOptions } from 'winston/lib/winston/transports';
@ArgsType()
class CustomersArgs {
  @Field(type => Int)
  @Min(0)
  skip: number = 0;

  @Field(type => Int)
  @Min(1)
  @Max(50)
  take: number = 25;

  @Field({ nullable: true })
  name?: string;
}

/**
 * Get all customers
 */
@Resolver()
export class CustomerResolver {
  @Query(returns => [UserDto])
  async getCustomers(@Args() { skip, take, name }: CustomersArgs) {
    let deleted = false;
    try {
      const query = getRepository(User)
        .createQueryBuilder('user')
        .where('(user.deleted = :deleted AND user.role = :role)', { deleted, role: UserRole.USER });

      if (name) {
        query.andWhere(
          new Brackets(qb => {
            qb.where("data->>'firstName' ILIKE :data", { data: '%' + name + '%' }).orWhere(
              "data->>'lastName' ILIKE :data",
              {
                data: '%' + name + '%',
              }
            );
          })
        );
      }

      query.take(take);
      query.skip(skip);
      query.orderBy('user.createdAt', 'DESC');

      const users: User[] = await query.getMany();

      const parsedUsers = users.map(user => {
        return toUserDto(user);
      });

      return parsedUsers;
    } catch (e) {
      throw Error('Failed');
    }
  }

  @Mutation(returns => User)
  async getCustomer(@Arg('id') id: string, @Ctx() context) {
    const {
      services: { UserService },
    } = context;
    const userService = new UserService();

    try {
      const user: User = await userService.getOne(id || 0);
      if (!user) {
        const message = 'The customer you are trying to get does not exist';
        throw new Error(message);
      }
      return toUserDto(user);
    } catch (e) {
      throw new Error('Failed to fetch customer');
    }
  }

  /**
   * Update customer
   */
  @Mutation(returns => SuccessFailureResponse)
  async updateCustomer(@Arg('id') id: string, @Arg('input') input: UpdateCustomerInput) {
    const userService = new UserService();
    const user: User = await userService.getOne(id);
    if (!user) {
      const message = 'The customer you are trying to update does not exist';
      throw new Error(message);
    }
    try {
      await userService.update(id, input);
      const message = 'The customer updated successfully';
      return successResponse(message);
    } catch (e) {
      throw new Error('Failed to update customer');
    }
  }

  /**
   * Delete customer
   */
  @Mutation(returns => SuccessFailureResponse)
  async deleteCustomer(@Arg('id') id: string) {
    const userService = new UserService();
    const userToRemove: User = await userService.getOne(id);
    if (!userToRemove) {
      const message = 'The customer you are trying to delete does not exist';
      throw new Error(message);
    } else {
      try {
        const message = 'The customer deleted successfully';
        await userService.delete(id);
        return successResponse(message);
      } catch (e) {
        throw new Error('Failed to delete customer');
      }
    }
  }

  @Mutation(() => SuccessFailureResponse)
  async updateCustomerSettings(@Arg('input') input: CustomerSettingsInput, @Ctx() context) {
    const user = context.getUser();
    const userService = new UserService();
    try {
      await userService.update(user.id, input);
      const message = 'The customer settings updated successfully';
      return successResponse(message);
    } catch (e) {
      throw new Error('Failed to update settings');
    }
  }

  /**
   * Create a customer
   */
  @Mutation(() => GenericResponse)
  async createCustomer(@Arg('input') input: CreateCustomerInput, @Ctx() context: IContext) {
    const { mailer } = context;
    const { email } = input;
    const userService = new UserService();
    const user = await userService.getBy({ email: email });

    if (user) {
      const message = 'User email already exist';
      throw new Error(message);
    }

    const userToBeSaved: User = new User();

    userToBeSaved.email = email;
    userToBeSaved.role = UserRole.USER;

    try {
      const customer = await userService.save(userToBeSaved);
      const token = generateToken(toUserDto(customer), '1h');
      await mailer.sendUserCreateEmail(token, email);

      return {
        customer: toUserDto(customer),
      };
    } catch (e) {
      throw new Error('Invitation email was not send');
    }
  }

  @Mutation(() => SuccessFailureResponse)
  async sendCustomerSupportEmail(@Arg('input') input: CustomerSupportInput, @Ctx() context) {
    const {
      services: { mailer },
    } = context;
    const { getToEmailAdress, ToEmail, sendSupportEmail } = mailer;
    const user = context.getUser();
    const { subject, emailMessage, userEmail } = input;
    try {
      const toEmail: typeof ToEmail = await getToEmailAdress(user);
      await sendSupportEmail(emailMessage, subject, userEmail, toEmail.email);
      const message = 'Support mail was sent.';
      return successResponse(message);
    } catch (e) {
      throw new Error('Email not sent');
    }
  }
}
