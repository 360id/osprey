import { Status } from '../entity/base-entity';
import { InputType, ObjectType, Field } from 'type-graphql';

@InputType()
export class UserDataInput {
  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field({ nullable: true })
  phoneNumber?: string;
}

@InputType()
export class UserInput {
  @Field()
  email: string;

  @Field()
  password: string;

  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field({ nullable: true })
  phoneNumber?: string;
}

@InputType()
export class CreateUserInput {
  @Field()
  email: string;

  @Field()
  organizationId: string;
}

@InputType()
export class UpdateUserInput {
  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  lastName?: string;
}

@InputType()
export class UpdateCustomerInput extends UpdateUserInput {}

@InputType()
export class CreateCustomerInput {
  @Field()
  email: string;
}

@InputType()
export class RegisterVerifierInput {
  @Field()
  masterKey: number;

  @Field()
  businessName: string;

  @Field()
  email: string;

  @Field()
  password: string;

  @Field()
  data?: UserDataInput;
}
@InputType()
export class IndividualVerifierInput {
  @Field()
  individualKey: number;

  @Field()
  businessName: string;

  @Field()
  email: string;

  @Field()
  password: string;

  @Field()
  data?: UserDataInput;
}

@InputType()
export class AuthInput {
  @Field()
  email: string;

  @Field()
  password: string;
}

@InputType()
export class ResetPasswordInput {
  @Field()
  token: string;

  @Field()
  password: string;
}

@InputType()
export class ForgotPasswordInput {
  @Field()
  email: string;
}

@InputType()
class CustomerSettings {
  @Field()
  isRecordPublic: boolean;
}

@InputType()
export class CustomerSettingsInput {
  @Field()
  settings: CustomerSettings;
}

@InputType()
export class CustomerSupportInput {
  @Field()
  subject: string;

  @Field()
  userEmail: string;

  @Field()
  emailMessage: string;
}

@InputType()
export class OrganizationDataInput {
  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  lastName?: string;
}
@InputType()
export class CreateOrganizationDataInput {
  @Field()
  email: string;

  @Field()
  firstName: string;

  @Field({ nullable: true })
  lastName?: string;
}

@InputType()
class OwnerInput {
  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field()
  email: string;

  @Field({ nullable: true })
  phoneNumber?: string;
}

@InputType()
class UpdateOwnerInput {
  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  lastName?: string;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  phoneNumber?: string;
}

@InputType()
class BusinessAddressInput {
  @Field()
  addressLine1: string;

  @Field({ nullable: true })
  addressLine2?: string;

  @Field()
  city: string;

  @Field()
  state: string;

  @Field()
  zip: string;
}

@InputType()
class UpdateBusinessAddressInput {
  @Field({ nullable: true })
  addressLine1?: string;

  @Field({ nullable: true })
  addressLine2?: string;

  @Field({ nullable: true })
  city?: string;

  @Field({ nullable: true })
  state?: string;

  @Field({ nullable: true })
  zip?: string;
}
@InputType()
export class CreateOrganizationInput {
  @Field()
  businessName: string;

  @Field()
  status: Status;

  @Field()
  masterKey: string;

  @Field()
  owner: OwnerInput;

  @Field()
  businessAddress: BusinessAddressInput;
}
@InputType()
export class UpdateOrganizationInput {
  @Field({ nullable: true })
  businessName: string;

  @Field({ nullable: true })
  status: Status;

  @Field({ nullable: true })
  masterKey: string;

  @Field({ nullable: true })
  owner: UpdateOwnerInput;

  @Field({ nullable: true })
  businessAddress: UpdateBusinessAddressInput;
}

@InputType()
export class RegisterAdminInput {
  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field()
  password: string;

  @Field()
  token: string;
}
