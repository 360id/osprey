import { AuthChecker } from 'type-graphql';

import { IContext } from './types/IContext';

// create auth checker function
export const authChecker: AuthChecker<IContext> = ({ context }, roles) => {
  const user = context.getUser();
  console.log('user', user);
  console.log('Roles', roles);
  if (roles.length === 0) {
    // if `@Authorized()`, check only if user exists
    return user !== undefined;
  }
  // there are some roles defined now

  if (!user) {
    // and if no user, restrict access
    return false;
  }
  if (roles.includes(user.role)) {
    // grant access if the roles overlap
    return true;
  }

  // no roles matched, restrict access
  return false;
};
