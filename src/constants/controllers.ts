export const responseStatuses: {
    [key: number]: {
        description: string
    }
} = {
    200: { description: 'success' },
    400: { description: 'bad request' },
    401: { description: 'unauthorized, missing/wrong jwt token' }
};
