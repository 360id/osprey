function toNotificationDto(notification) {
    return {
        id: notification.id,
        createdAt: notification.createdAt,
        updatedAt: notification.updatedAt,
        messageText: notification.messageText,
        hasUntilDate: notification.hasUntilDate,
        untilDate: notification.untilDate,
        requestsFrequency: notification.requestsFrequency,
        audienceType: notification.audienceType,
        isActive: notification.isActive,
        startDate: notification.startDate,
        lastSendDate: notification.lastSendDate
    };
}
export { toNotificationDto };
