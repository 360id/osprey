import { User } from 'src/entity';

interface UserDto {
  id: string;
  email: string;
  createdAt: Date;
  updatedAt: Date;
  role: string;
  status: string;
  organizationId?: string | undefined;
  individualKey?: string | undefined;
  isOnboarding?: boolean | undefined;
  firstName?: string | undefined;
  lastName?: string | undefined;
}
function toUserDto(user: User) {
  return {
    id: user.id,
    email: user.email,
    createdAt: user.createdAt,
    updatedAt: user.updatedAt,
    role: user.role,
    status: user.status,
    organizationId: user.organizationId,
    individualKey: user.individualKey,
    isOnboarding: user.isOnboarding,
    firstName: user.firstName,
    lastName: user.lastName,
    phoneNumber: user.phoneNumber,
  };
}
export { toUserDto, UserDto };
