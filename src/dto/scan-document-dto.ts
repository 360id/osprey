function toScanDocumentDto(scanDocument) {
  const {
    id,
    data,
    userId,
    rapidIdVerification
  } = scanDocument;

  return {
    id,
    data,
    userId,
    rapidIdVerification
  };
}
export { toScanDocumentDto };
