import { Status } from '../entity/base-entity';
import { User } from '../entity';
import { Organization } from '../entity/organization';
interface OrganizationDto {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  ownerId: number;
  status: Status;
  businessName: string;
  users: User[];
  masterKey: string;
}

function toOrganizationDto(organization: Organization) {
  return {
    id: organization.id,
    createdAt: organization.createdAt,
    updatedAt: organization.updatedAt,
    ownerId: organization.ownerId,
    status: organization.status,
    businessName: organization.businessName,
    users: organization.users,
    masterKey: organization.masterKey,
  };
}
export { toOrganizationDto, OrganizationDto };
