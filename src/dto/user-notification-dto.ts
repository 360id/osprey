function toUserNotificationDto(userNotification) {
    return {
        id: userNotification.id,
        createdAt: userNotification.createdAt,
        updatedAt: userNotification.updatedAt,
        sendDate: userNotification.sendDate,
        isSeen: userNotification.isSeen,
        notification: userNotification.notification
    };
}
export { toUserNotificationDto };
