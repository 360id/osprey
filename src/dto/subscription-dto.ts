function toSubscriptionDto(subscription) {
    return {
        id: subscription.id,
        price: subscription.price,
        createdAt: subscription.createdAt,
        updatedAt: subscription.updatedAt,
        subscriptionName: subscription.subscriptionName,
        paymentCycle: subscription.paymentCycle,
        autoRenew: subscription.autoRenew,
        isActive: subscription.isActive,
    };
}
export { toSubscriptionDto };
