import { createConnection } from 'typeorm';
import { config } from './config';
import passport from 'passport';
import { passportConfig } from './auth';
import 'reflect-metadata';
import express, { Application } from 'express';
import session from 'express-session';
import { v4 as uuidv4 } from 'uuid';
import cors from 'cors';
import redis from 'redis';
import { createGraphQLServer } from './graphQLServer';
import connectRedis from 'connect-redis';

const RedisStore = connectRedis(session);
const options = {
  host: 'redis',
  port: 6379,
};
const redisClient = redis.createClient(options);

async function initializeDatabase() {
  return await createConnection({
    name: 'default',
    type: 'postgres',
    host: config.databaseHost,
    port: +config.databasePort,
    username: config.databaseUser,
    password: config.databasePassword,
    database: config.databaseName,
    synchronize: true,
    logging: false,
    entities: config.dbEntitiesPath,
    migrationsTableName: config.migrationsTableName,
    migrations: config.migrations,
    cli: config.cli,
    extra: {
      ssl: config.dbsslconn, // if not development, will use SSL
    },
  }).then(() => {
    console.log('Database connection established');
  });
}

const main = async () => {
  //Initialize database
  await initializeDatabase();

  passportConfig();

  const app: Application = express();

  // Provides important security headers to make your app more secure
  // app.use(helmet());

  // sessions
  // app.keys = [config.jwtSecret];
  // app.use(session(app));
  // const sessionOpts = {
  //   genid: req => uuidv4(),
  //   saveUninitialized: true,
  //   secret: 'something',
  //   resave: true,
  // };
  // app.use(session(sessionOpts));
  app.use(
    session({
      name: '_buzzard',
      store: new RedisStore({ client: redisClient, disableTouch: true }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 * 10, //10 years
        httpOnly: true,
        sameSite: 'lax',
        // secure: __prod__,//set true for production
        secure: false,
      },
      saveUninitialized: false,
      secret: 'keyboard cat',
      resave: false,
    })
  );

  // Enable cors with default options
  const corsOptions = {
    origin: 'http://localhost:3000',
    credentials: true,
  };
  app.use(cors(corsOptions));

  // Logger middleware -> use winston as logger (logging.ts with config)
  // app.use(logger(winston));

  // app.use(express.json());
  // app.use(
  //   express.urlencoded({
  //     extended: true,
  //   })
  // );

  // app.use(cookieParser());
  // authentication

  app.use(passport.initialize());
  app.use(passport.session());

  const graphQLServer = await createGraphQLServer();

  graphQLServer.applyMiddleware({ app, cors: false });

  // these routes are NOT protected by the JWT middleware, also include middleware to respond with "Method Not Allowed - 405".
  // app.use(unprotectedRouter.routes()).use(unprotectedRouter.allowedMethods());

  // JWT middleware -> below this line routes are only reached if JWT token is valid, secret as env variable
  // do not protect swagger-json and swagger-html endpoints
  // app.use(jwt({ secret: config.jwtSecret }).unless({ path: [/(\/api)?\/swagger-/] }));

  // These routes are protected by the JWT middleware, also include middleware to respond with "Method Not Allowed - 405".
  // app.use(protectedRouter.routes()).use(protectedRouter.allowedMethods());

  app.listen(config.port);

  console.log(`🚀 GraphQL Server ready at http://localhost:${config.port}${graphQLServer.graphqlPath}`);
};

main();
