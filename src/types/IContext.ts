import { Request as ExpressRequest } from 'express';
import { PassportContext } from 'graphql-passport';
import { User } from '../entity/user';
import { EmailService } from '../services/EmailService';

type Auth = {
  email: string;
  password: string;
};
export interface IContext extends PassportContext<User, Auth, ExpressRequest> {
  mailer: EmailService;
}
