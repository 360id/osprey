import { Factory, Seeder } from 'typeorm-seeding';
import { Connection, getRepository } from 'typeorm';
import { Subscription } from '../entity/subscription';

export default class CreateSubscription implements Seeder {
    public async run( factory: Factory, connection: Connection): Promise<any> {

        const data = JSON.stringify({
            name: 'flat',
            baseFee: {
                fee: 800,
                currency: '$'
            },
            ranges: [
                {
                    min: 0,
                    max: 1000,
                    pricePearUnit: 1,
                    currency: '$'
                },
                {
                    min: 1000,
                    max: 10000,
                    pricePearUnit: 0.50,
                    currency: '$'
                },
                {
                    min: 10000,
                    pricePearUnit: 0.20,
                    currency: '$'
                }
            ]
        });

        await getRepository(Subscription).createQueryBuilder('subscriptions')
            .insert()
            .into(Subscription)
            .values([
                {
                    data: () => `'${data}'`
                }
            ])
            .execute();
    }
}
