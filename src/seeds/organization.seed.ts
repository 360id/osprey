import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Organization } from '../entity/organization';

export default class CreateOrganization implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(Organization)().createMany(100);
  }
}
