import { Factory, Seeder } from 'typeorm-seeding';
import { Connection, getRepository } from 'typeorm';
import { User, UserRole } from '../entity/user';
import * as argon2 from 'argon2';

export default class CreateUser implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const password = await argon2.hash('super_admin');
    await getRepository(User)
      .createQueryBuilder('users')
      .insert()
      .into(User)
      .values([
        {
          role: UserRole.SUPER_ADMIN,
          email: 'super_admin@gmail.com',
          password: password,
          firstName: 'Super',
          lastName: 'Admin',
          phoneNumber: '0422354783',
        },
      ])
      .execute();

    await factory(User)().createMany(100);
  }
}
