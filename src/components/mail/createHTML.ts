import Maizzle from '@maizzle/framework';
import { getTemplate } from './getTemplate';
import { EmailEventModel } from './Mail';

const createHtml = async ({ template, context }: EmailEventModel) => {
  const newTemplate = await getTemplate(template);
  return Maizzle.render(newTemplate, {
    tailwind: {
      config: require('./tailwind.config'),
    },
    maizzle: { context, env: 'node', inlineCSS: true },
  })
    .then(({ html }) => {
      return html;
    })
    .catch(error => console.log(error));
};

export { createHtml };
