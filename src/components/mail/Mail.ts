import * as nodemailer from 'nodemailer';
import { config } from '../../config';
import Logger from '../logger/Logger';
import hbs from 'nodemailer-express-handlebars';
import { createHtml } from './createHTML';
export interface EmailEventModel {
  to: string;
  from?: string;
  subject: string;
  cc?: string | string[];
  bcc?: string | string[];
  template: string;
  context?: any;
  attachments?: any[];
}

export class Mail {
  private static transporter: any;

  constructor() {
    Mail.transporter = Mail.createTransporter(config.email);
  }

  private static createTransporter(config) {
    return nodemailer.createTransport({
      host: config.host,
      port: config.port,
      secure: false,
      auth: {
        user: config.auth.user,
        pass: config.auth.pass,
      },
      tls: {
        rejectUnauthorized: false,
      },
    });
  }

  public async send(data: EmailEventModel) {
    console.log('data', data);
    const mailOps = data as nodemailer.SendMailOptions;
    mailOps.from = config.email.from;
    mailOps['context']['basePath'] = config.env.basePath;

    const handlebarOptions = {
      viewEngine: {
        extName: '.hbs',
        partialsDir: 'src/components/mail/views',
        defaultLayout: undefined,
      },
      viewPath: 'src/components/mail/views',
      extName: '.hbs',
    };

    const html = await createHtml(data);

    Mail.transporter.use('compile', hbs(handlebarOptions));
    const finalOpts = { ...mailOps, html: html };
    console.log('MailIOs', finalOpts);
    await new Promise((resolve, reject) => {
      Mail.transporter.sendMail(finalOpts, (error, info) => {
        if (error) {
          Logger.log('nodemailer error: ', error);
          reject(error);
          return;
        }
        resolve(info);
      });
    });
  }
}
