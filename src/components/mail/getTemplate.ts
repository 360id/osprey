import fs from 'fs';
import path from 'path';

const getTemplate = async (template: string) => {
  const filePath = path.resolve(__dirname, `./templates/${template}.html`);
  try {
    const data = await fs.promises.readFile(filePath, 'utf-8');
    return data.toString();
  } catch (e) {
    console.log(e);
  }
};

export { getTemplate };
