import Stripe from 'stripe';
import { config } from '../../config';

export class StripeComponent {
  private static instance: Stripe;

  public static getInstance() {
    if (!StripeComponent.instance) {
      StripeComponent.instance = new Stripe(config.stripe.secretKey, {
        apiVersion: '2020-08-27', // Taken from https://stripe.com/docs/api/versioning
      });
    }
    return StripeComponent.instance;
  }

  public static async createCustomer(params: Stripe.CustomerCreateParams) {
    const customer: Stripe.Customer = await StripeComponent.getInstance().customers.create(params);
    return customer;
  }

  public async retrieveCustomer(id: string, params: Stripe.CustomerRetrieveParams) {
    const customer: Stripe.Customer | Stripe.DeletedCustomer = await StripeComponent.getInstance().customers.retrieve(id, params);
    return customer;
  }

  private static async createToken(params: Stripe.TokenCreateParams) {
    const token: Stripe.Token = await StripeComponent.getInstance().tokens.create(params);
    return token;
  }

  public async addCardToCustomer(params: Stripe.TokenCreateParams, id: string) {
    if (!id) {
      const customer: Stripe.Customer = await StripeComponent.createCustomer(params);
      id = customer.id;
    }

    const token: Stripe.Token = await StripeComponent.createToken(params);

    const sourceParams: Stripe.CustomerSourceCreateParams = { source: token.id };

    const customerSource: Stripe.CustomerSource = await StripeComponent.getInstance().customers.createSource(id, sourceParams);
    return customerSource;
  }

  public static async createSubscription(params: Stripe.SubscriptionCreateParams) {
   return await StripeComponent.getInstance().subscriptions.create(params);
  }

  public static async createPrices(params: Stripe.PriceCreateParams) {
    const subscription: Stripe.Price = await StripeComponent.getInstance().prices.create(params);
    return subscription;
  }

  public static async retrieveSubscription(id: string) {
    const subscription: Stripe.Subscription = await StripeComponent.getInstance().subscriptions.retrieve(id);
    return subscription;
  }

  public static async updateSubscription(id: string, params: Stripe.SubscriptionUpdateParams) {
    const subscription: Stripe.Subscription = await StripeComponent.getInstance().subscriptions.update(id, params);
    return subscription;
  }

  public static async cancelSubscription(id: string) {
    const subscription: Stripe.Subscription = await StripeComponent.getInstance().subscriptions.del(id);
    return subscription;
  }

  public static async getSubscriptionsList(params: Stripe.SubscriptionListParams) {
    const subscriptions: Stripe.ApiList<Stripe.Subscription> = await StripeComponent.getInstance().subscriptions.list(params);
    return subscriptions;
  }

  public static async createUsageRecord(id, params: Stripe.UsageRecordCreateParams) {
      return await StripeComponent.getInstance().subscriptionItems.createUsageRecord(
          id,
          {
              quantity: params.quantity,
              timestamp: params.timestamp
          },
      );
  }
}
