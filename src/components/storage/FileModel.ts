export interface FileModel {
  fileName: string;
  fileType: string;
  size: number;
  uid: number;
  randomString?: string;
}
