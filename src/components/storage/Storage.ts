import StorageAdapter from './StorageAdapter';
import { FileModel } from './FileModel';

export default class Storage {
  private static instance: any = StorageAdapter.getInstance();

  private constructor() {
    Storage.instance ? Storage.instance : (Storage.instance = StorageAdapter.getInstance());
  }

  public static async save(file: any, fileOpts: FileModel): Promise<any> {
    return await Storage.instance.write(file, fileOpts);
  }

  static async update(file: any, filePath: string): Promise<any> {
    return await Storage.instance.update(file, filePath);
  }

  static async delete(filePath: string): Promise<boolean> {
    return await Storage.instance.delete(filePath);
  }
}
