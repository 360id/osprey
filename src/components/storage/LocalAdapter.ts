import * as path from 'path';
import * as fs from 'fs';
import { IAdapter } from './IAdapter';
import { config } from '../../config';
import { FileModel } from './FileModel';

export default class LocalAdapter implements IAdapter {
  public async write(file: any, fileOpts: FileModel) {
    return new Promise(async (resolve, reject) => {
      const dir = fileOpts.randomString
        ? `${config.storage.local.dir}/${fileOpts.uid}/${fileOpts.randomString}/${fileOpts.fileType}`
        : `${config.storage.local.dir}/${fileOpts.uid}/${fileOpts.fileType}`;
      const filePath = `${dir}/${this.parseFileName(fileOpts.fileName)}.${fileOpts.fileType}`;
      const servePath = filePath.replace(config.storage.local.dir, '');
      try {
        await this.createDir(dir);

        fs.writeFile(filePath, file instanceof Buffer ? file : fs.readFileSync(file), (err) => {
          if (err) return reject(err);
          return resolve({
            absPath: `${process.env.PWD}/${filePath}`,
            relativePath: `${filePath}`,
            dir: dir,
            servePath: servePath
          });
        });
      } catch (err) {
        return reject(err);
      }
    });
  }

  public async update(file: any, filePath: string) {
    return new Promise((resolve, reject) => {
      fs.stat(filePath, (err) => {
        if (err) return reject(`File doesn't exist or doesn't have correct write permissions: ${filePath}`);

        fs.writeFile(filePath, file instanceof Buffer ? file : fs.readFileSync(file), (err) => {
          if (err) return reject(err);
          return resolve({
            absPath: `${process.env.PWD}/${filePath}`,
            relativePath: `${filePath}`
          });
        });
      });
    });
  }

  public async delete(filePath: string) {
    return new Promise((resolve, reject) => {
      fs.stat(filePath, (err) => {
        if (err) return reject(`File doesn't exist or doesn't have correct write permissions: ${filePath}`);

        fs.unlink(filePath, (err) => {
          if (err) return reject(err);
          return resolve(true);
        });
      });
    });
  }

  private parseFileName(fileName) {
    // TODO: Optionally handle the extension parsing a bit better :\
    return fileName.split('.')[0] || fileName;
  }

  private async createDir(targetDir: String) {
    targetDir.split('/').forEach((dir, index, splits) => {
      const parent = splits.slice(0, index).join('/');
      const dirPath = path.resolve(parent, dir);
      if (!fs.existsSync(dirPath)) {
        fs.mkdirSync(dirPath);
      }
    });
  }
}
