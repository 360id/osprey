import { config } from "../../config";
import LocalAdapter from "./LocalAdapter";

export default class StorageAdapter {
  private static instance: StorageAdapter;

  private static createAdapter() {
    switch (config.storage.default) {
      case "local": {
        return new LocalAdapter();
      }
    }
    throw new Error("Unsupported adapter " + config.storage.default);
  }

  public static getInstance() {
    if (!StorageAdapter.instance) {
      StorageAdapter.instance = StorageAdapter.createAdapter();
    }
    return StorageAdapter.instance;
  }
}
