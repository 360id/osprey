import { FileModel } from './FileModel';

export interface IAdapter {
  write(file: any, fileOpts: FileModel): Promise<any>;

  update(file: any, filePath: string): Promise<any>;

  delete(filePath: string): Promise<any>;
}
