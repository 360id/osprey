import * as winston from 'winston';

export default class Logger {
  private static instance: any;

  private static setInstance() {
    if (!Logger.instance) {
      Logger.instance = Logger.createLogger();
    }
  }

  private static createLogger() {
    return winston.createLogger({
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
          handleExceptions: true
        }),
        new winston.transports.File({
          filename: 'debug.log',
          format: winston.format.simple(),
          handleExceptions: true
        })
      ],
      level: 'debug',
      exitOnError: false
    });
  }

  public static log(...args: any[]): void {
    Logger.setInstance();
    Logger.instance.log('debug', ...args);
  }
}
