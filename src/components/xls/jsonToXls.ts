const xlsx = require('xlsx');

export function jsonToExel(body: any[]) {
    const newWB = xlsx.utils.book_new();
    const testData = { Date: '2016-04-02T05:00:00.000Z', Region: 'Apr', Brand: 'Helfiger', State: 'OH', Net: 74.84 };
    const newWS = xlsx.utils.json_to_sheet([testData]);
    xlsx.utils.book_append_sheet(newWB, newWS, 'new Data');

    const wopts = { bookType: 'xlsx', bookSST: false, type: 'buffer'};
    return xlsx.write(newWB, wopts);
}
