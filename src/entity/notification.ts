import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './base-entity';
import { UserNotification } from './user-notification';

export enum AudienceTypes {
    USER = 'users',
    ADMIN_VERIFIER = 'admin_verifiers',
    INDIVIDUAL_VERIFIER = 'individual_verifiers',
    ALL = 'all_users'
}

export enum RequestsFrequency {
    MONTHLY = 'monthly',
    DAILY = 'daily',
    WEEKLY = 'weekly',
    YEARLY = 'yearly',
    ONETIME = 'onetime',
    RECURRING = 'recurring'
}

@Entity()
export class Notification  extends BaseEntity {

    @Column({ length: 100 })
    messageText: string;

    @Column({ type: 'date' })
    startDate: Date;

    @Column({ type: 'date', nullable: true })
    lastSendDate: Date;

    @Column({ type: 'boolean', default: true })
    isActive: boolean;

    @Column({ type: 'varchar', default: 'draft' })
    status: string;

    @Column({
        type: 'boolean',
        default: false
    })
    hasUntilDate: boolean;

    @Column({ type: 'date', nullable: true })
    untilDate: Date;

    @Column({
        type: 'enum',
        enum: RequestsFrequency,
        default: RequestsFrequency.MONTHLY
    })
    requestsFrequency: RequestsFrequency;

    @Column({
        type: 'enum',
        enum: AudienceTypes,
        default: AudienceTypes.ALL
    })
    audienceType: AudienceTypes;

    @OneToMany(type => UserNotification, userNotification => userNotification.notification, { onDelete: 'CASCADE' })
    userNotifications!: UserNotification[];
}

export const notificationCreateSchema = {
    messageText: { type: 'string', required: true, example: 'some text' },
    startDate: { type: 'date', required: true, example: 'start date' },
    hasUntilDate: { type: 'boolean', required: false, example: 'boolean' },
    untilDate: { type: 'date', required: false, example: 'until notification date if hasUntil date is true' },
    audienceType: { type: 'enum', example: 'enum users, admin_verifiers individual_verifiers or all_users' },
    requestsFrequency: { type: 'enum', example: 'enum recurring monthly, daily, weekly, yearly or onetime' }
};

export const notificationUpdateSchema = {
    messageText: { type: 'string', example: 'some text' },
    startDate: { type: 'date', example: 'start date' },
    hasUntilDate: { type: 'boolean', example: 'boolean' },
    untilDate: { type: 'date', example: 'until notification date if hasUntil date is true' },
    audienceType: { type: 'enum', example: 'enum users, admin_verifiers individual_verifiers or all_users' },
    requestsFrequency: { type: 'enum', example: 'enum monthly, daily, weekly, yearly or onetime' }
};
