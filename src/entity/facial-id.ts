import { Entity } from 'typeorm';
import { BaseEntity } from './base-entity';

@Entity()
export class FacialId extends BaseEntity {}
