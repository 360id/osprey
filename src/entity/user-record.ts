import { Entity } from 'typeorm';
import { BaseEntity } from './base-entity';

@Entity()
export class UserRecord extends BaseEntity {}
