import { Column, Entity, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { User } from './user';
import { BaseEntity, Status } from './base-entity';
import { OrganizationSubscription } from './organziation-subscribtion';
import { ObjectType, Field, registerEnumType } from 'type-graphql';

@ObjectType()
class Address {
  @Field(type => String)
  addressLine1: string;

  @Field(type => String, { nullable: true })
  addressLine2?: string;

  @Field(type => String)
  city: string;

  @Field(type => String)
  state: string;

  @Field(type => String)
  zip: string;

  @Field(type => String, { nullable: true })
  country?: string;
}

@Entity()
@ObjectType()
export class Organization extends BaseEntity {
  @Field()
  @Column({ type: 'varchar', length: 200 })
  masterKey: string;

  @Field()
  @Column({ type: 'varchar', length: 200 })
  businessName: string;

  @Field(type => Address)
  @Column({ type: 'jsonb', nullable: true })
  businessAddress: Address;

  @Field(type => Status)
  @Column({
    type: 'enum',
    enum: Status,
    default: Status.DRAFT,
  })
  status: Status;

  @Field()
  @Column({ nullable: true, unique: true })
  stripeSubscriptionId: string;

  @Field()
  @Column({ nullable: true })
  ownerId: string;

  @Field(type => User)
  @OneToOne(type => User, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'ownerId' })
  owner: User;

  @Field(type => [User])
  @OneToMany(() => User, user => user.organization, { onDelete: 'CASCADE' })
  users: User[];

  @Field(type => [OrganizationSubscription])
  @OneToMany(type => OrganizationSubscription, organizationSubscription => organizationSubscription.organization, {
    onDelete: 'CASCADE',
  })
  organizationSubscriptions!: OrganizationSubscription[];
}

// export const orgSchema = {
//   businessName: { type: 'string', required: false, example: 'bank name' },
//   masterKey: { type: 'string', required: false, example: 'ganereted key' },
//   status: { type: 'enum', required: false, example: 'inactive' },
//   data: {
//     type: 'object',
//     example: 'json data related to user and organization for example first name, last name, email',
//   },
// };
