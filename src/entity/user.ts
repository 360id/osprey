import { BeforeInsert, Column, Entity, JoinColumn, ManyToOne, OneToMany, Generated } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { BaseEntity, Status } from './base-entity';
import { Organization } from './organization';
import { UserNotification } from './user-notification';
import { ScanDocument } from './scan-document';
import { ObjectType, Field, registerEnumType } from 'type-graphql';
import * as argon2 from 'argon2';

export enum UserRole {
  SUPER_ADMIN = 'super_admin',
  USER = 'user',
  ADMIN_VERIFIER = 'verifier_admin',
  INDIVIDUAL_VERIFIER = 'individual_verifier',
}

@ObjectType({ description: 'User data' })
export class UserData {
  @Field()
  firstName: String;
}

@ObjectType()
class Settings {
  @Field(type => String)
  name: number;
}

registerEnumType(UserRole, {
  name: 'UserRole',
});

@Entity()
@ObjectType()
export class User extends BaseEntity {
  @Field()
  @Column({ length: 100, nullable: true })
  firstName: string;

  @Field()
  @Column({ length: 100, nullable: true })
  lastName: string;

  @Field()
  @Column({ length: 100, unique: true })
  email: string;

  @Field()
  @Column({ nullable: true })
  phoneNumber: string;

  @Field()
  @Column({ nullable: true })
  masterKey: string;

  @Field()
  @Column({ nullable: true, unique: true })
  individualKey: string;

  @Field(type => Settings)
  @Column({ type: 'jsonb', nullable: true })
  settings: Settings;

  @Field(() => Boolean)
  @Column({
    type: 'boolean',
    default: false,
  })
  isOnboarding: boolean;

  @Field(type => Status)
  @Column({
    type: 'enum',
    enum: Status,
    default: Status.DRAFT,
  })
  status: Status;

  @Field(type => UserRole)
  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.USER,
  })
  role: UserRole;

  @Field({ nullable: true })
  @Column({ nullable: true })
  organizationId: string;

  @ManyToOne(() => Organization, organization => organization.users, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Organization;

  @Column({ type: 'text', nullable: true })
  password: string;

  @Column({ type: 'text', nullable: true })
  resetPasswordToken: string;

  @Column({ nullable: true })
  resetPasswordExpire: string;

  @OneToMany(type => UserNotification, userNotification => userNotification.user, { onDelete: 'CASCADE' })
  userNotifications!: UserNotification[];

  @OneToMany(type => ScanDocument, scanDocument => scanDocument.user, {
    onDelete: 'CASCADE',
  })
  userScans!: ScanDocument[];

  @BeforeInsert()
  async hashPassword() {
    if (this.password) {
      this.password = await argon2.hash(this.password);
    }
  }

  async comparePassword(attempt: string) {
    return await argon2.verify(this.password, attempt);
  }
}

// export const createUserSchema = {
//   email: { type: 'string', required: true, example: 'schyles@360id.online' },
//   data: { type: 'object', example: 'json data related to user' },
// };

// export const updateUserSchema = {
//   email: { type: 'string', required: false, example: 'schyles@360id.online' },
//   data: { type: 'object', example: 'json data related to user' },
// };

// export const updateUserSettings = {
//   settings: {
//     type: 'object',
//     example: 'json data related to user settings for example is record public or private',
//   },
// };

// export const authSchema = {
//   email: { type: 'string', required: true, example: 'schyles@360id.online' },
//   password: { type: 'string', required: true, example: 'password' },
//   confirmPassword: { type: 'string', required: true, example: 'password' },
//   data: {
//     type: 'object',
//     example: 'json data related to user for example first name, last name',
//   },
// };

// export const verifierAdminAuthSchema = {
//   email: { type: 'string', required: true, example: 'schyles@360id.online' },
//   password: { type: 'string', required: true, example: 'password' },
//   confirmPassword: { type: 'string', required: true, example: 'password' },
//   data: {
//     type: 'object',
//     example: 'json data related to user for example first name, last name',
//   },
// };

// export const verifierIndividualAuthSchema = {
//   email: { type: 'string', required: true, example: 'schyles@360id.online' },
//   password: { type: 'string', required: true, example: 'password' },
//   confirmPassword: { type: 'string', required: true, example: 'password' },
//   data: {
//     type: 'object',
//     example: 'json data related to user for example first name, last name',
//   },
// };

// export const loginSchema = {
//   email: { type: 'string', required: true, example: 'schyles@360id.online' },
//   password: { type: 'string', required: true, example: 'password' },
// };
// export const supportBody = {
//   userEmail: {
//     type: 'string',
//     required: true,
//     example: 'schyles@360id.online',
//   },
//   subject: { type: 'string', required: true, example: 'email subject' },
//   emailMessage: { type: 'string', required: true, example: 'email message' },
// };
