import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base-entity';
import { User } from './user';

@Entity()
export class ScanDocument extends BaseEntity {
    @Column({nullable: true})
    userId: string;

    @Column({ type: 'jsonb', nullable: true })
    rapidIdVerification: any;

    @ManyToOne(() => User, user => user.userScans, { onDelete: 'CASCADE'})
    user: User;
}

export const createScanDocument = {
    data: { type: 'object', example: 'json data related to the document' }
};

export const updateScanDocument = {
    data: { type: 'object', example: 'json data related to the document' }
};
