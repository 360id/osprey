import { Field, ObjectType } from 'type-graphql';
import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './base-entity';
import { OrganizationSubscription } from './organziation-subscribtion';

@ObjectType()
class Data {
  @Field()
  subscription: string;
}
@Entity()
@ObjectType()
export class Subscription extends BaseEntity {
  @Field()
  @Column({
    type: 'boolean',
    default: false,
  })
  isActive: boolean;

  @Field({ nullable: true })
  @Column({ nullable: true, unique: true })
  stripeCustomerId: string;

  @Field(() => Data, { nullable: true })
  @Column({ type: 'jsonb', nullable: true })
  data: any;

  @Field(type => [OrganizationSubscription])
  @OneToMany(type => OrganizationSubscription, organizationSubscription => organizationSubscription.subscription, {
    onDelete: 'CASCADE',
  })
  organizationSubscriptions!: OrganizationSubscription[];
}
