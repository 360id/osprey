import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from './base-entity';
import { Subscription } from './subscription';
import { Organization } from './organization';
import { Field, ObjectType } from 'type-graphql';

@Entity()
@ObjectType()
export class OrganizationSubscription extends BaseEntity {
  @Field()
  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  subscriptionStartDate: string;

  @Field({ nullable: true })
  @Column({ type: 'date', nullable: true })
  subscriptionEndDate: Date;

  @Field()
  @Column({ type: 'boolean', default: false })
  isActive: boolean;

  @Field({ nullable: true })
  @Column({ nullable: true })
  subscriptionId: number;

  @Field(type => Subscription)
  @ManyToOne(type => Subscription, subscription => subscription.organizationSubscriptions, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'subscriptionId' })
  subscription: Subscription;

  @Field({ nullable: true })
  @Column({ nullable: true })
  organizationId: number;

  @Field(type => Organization)
  @ManyToOne(type => Organization, organization => organization.organizationSubscriptions, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'organizationId' })
  organization: Organization;
}
