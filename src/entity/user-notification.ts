import {
    Column,
    Entity,
    JoinColumn, ManyToOne,
} from 'typeorm';
import { BaseEntity } from './base-entity';
import { User } from './user';
import { Notification } from './notification';


@Entity()
export class UserNotification extends BaseEntity {

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    sendDate: string;

    @Column({
        type: 'boolean',
        default: false
    })
    isSeen: boolean;

    @Column({nullable: true})
    userId: number;
    @ManyToOne(type => User, user => user.userNotifications, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'userId' })
    user: User;

    @Column({nullable: true})
    notificationId: number;
    @ManyToOne(type => Notification, notification => notification.userNotifications, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'notificationId' })
    notification: Notification;
}
