import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Field, ObjectType, registerEnumType } from 'type-graphql';

export enum Status {
  DRAFT = 'draft',
  INACTIVE = 'inactive',
  ACTIVE = 'active',
}

registerEnumType(Status, {
  name: 'Status',
  description: 'status',
});

@Entity()
@ObjectType()
export class BaseEntity {
  @Field()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @Column({
    type: 'boolean',
    default: false,
  })
  deleted: boolean;

  @Field()
  @CreateDateColumn()
  createdAt: Date;

  @Field()
  @UpdateDateColumn()
  updatedAt: Date;
}
