import {
    Column,
    Entity,
} from 'typeorm';
import { BaseEntity } from './base-entity';


@Entity()
export class SnsUser extends BaseEntity {

    @Column({ nullable: true })
    pushToken: string;

    @Column({ nullable: true })
    awsEndpoint: string;

    @Column({ nullable: true })
    deviceId: string;

    @Column({ nullable: true })
    userId: number;
}

export const createSnsEnpoint = {
    deviceId: { type: 'string', example: 'divice id' },
    token: { type: 'string', example: 'token frm FCM' },
    platform: { type: 'string', example: 'ios or android' },
};
