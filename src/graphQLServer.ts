import { buildContext } from 'graphql-passport';
import * as entity from './entity';
import * as services from './services';
import { ApolloServer } from 'apollo-server-express';
import { UserResolver } from './resolvers/userResolver';
import { AuthResolver } from './resolvers/authResolver';
import { CustomerResolver } from './resolvers/customerResolver';
import { buildSchema } from 'type-graphql';
import { OrganizationResolver } from './resolvers/organizationResolver';
import { mailer } from './services';
import { authChecker } from './auth-checker';
//Integrate GraphQL server
const createSchema = async () =>
  buildSchema({
    resolvers: [UserResolver, AuthResolver, CustomerResolver, OrganizationResolver],
    authChecker,
  });

const BASIC_LOGGING = {
  requestDidStart(requestContext) {
    console.log('request started');
    // console.log(requestContext.request.query);
    console.log('Variables', requestContext.request.variables);
    return {
      didEncounterErrors(requestContext) {
        console.log('an error happened in response to query ' + requestContext.request.query);
        console.log(requestContext.errors);
      },
    };
  },

  willSendResponse(requestContext) {
    console.log('response sent', requestContext.response);
  },
};

const graphqlContext = async ({ req, res }) =>
  buildContext({
    req,
    res,
    mailer,
  });

async function createGraphQLServer() {
  return new ApolloServer({
    schema: await createSchema(),
    context: graphqlContext,
    plugins: [BASIC_LOGGING],
    formatError: e => {
      //Intercept errors here.
      return e;
    },
  });
}

export { createGraphQLServer, createSchema, graphqlContext };
