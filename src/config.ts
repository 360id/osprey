import dotenv from 'dotenv';

dotenv.config({ path: '.env' });

export interface IConfig {
  port: number;
  debugLogging: boolean;
  dbsslconn: boolean;
  jwtSecret: string;
  jwtExpiration: number;
  databaseHost: string;
  databaseName: string;
  databaseUser: string;
  databasePassword: string;
  databasePort: number;
  dbEntitiesPath: string[];
  migrationsTableName: string;
  migrations: string[];
  seeds: string[];
  entities: string[];
  factories: string[];
  cli: any;
  email: any;
  env: any;
  resetPasswordUrl: string;
  storage: any;
  stripe: {
    publicKey: string;
    secretKey: string;
  };
  rapidId: {
    apiUrl: string;
    authToken: string;
  };
  snsPushNotification: {
    region: string;
    apiVersion: string;
    snsAndroidArn: string;
    snsIosArn: string;
    accessKeyId: string;
    accessSecretKey: string;
    topicArn: string;
  };
}

const isDevMode = process.env.NODE_ENV == 'development';
const DATABASE_URL = `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@`;

const config: IConfig = {
  port: +process.env.SERVER_PORT || 8000,
  debugLogging: isDevMode,
  dbsslconn: false,
  jwtSecret: process.env.JWT_SECRET || 'your-secret-whatever',
  jwtExpiration: new Date().setDate(new Date().getDate() + 1),
  databaseHost: process.env.DB_HOST,
  databaseName: process.env.DB_DATABASE_NAME,
  databaseUser: process.env.DB_USER,
  databasePassword: process.env.DB_PASSWORD,
  databasePort: +process.env.DB_PORT,
  dbEntitiesPath: [...(isDevMode ? ['src/entity/**/*.ts'] : ['build/src/entity/**/*.js'])],
  migrationsTableName: 'custom_migration_table',
  migrations: ['migrations/**/*{.ts,.js}'],
  seeds: ['src/seeds/**/*.ts'],
  entities: [__dirname + '/entity/**/*{.ts,.js}'],
  factories: [__dirname + '/factories/**/*{.ts,.js}'],
  cli: {
    migrationsDir: 'migrations',
  },
  email: {
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: process.env.MAIL_SSL,
    auth: {
      user: process.env.MAIL_USERNAME,
      pass: process.env.MAIL_PASSWORD,
    },
    from: 'noreply@360id.online',
  },
  env: {
    basePath: process.env.APP_URL,
  },
  resetPasswordUrl: process.env.RESET_PASSWORD_URL,
  storage: {
    default: process.env.APP_STORAGE || 'local',
    local: {
      adapter: 'local',
      dir: 'static',
      url: '/static',
    },
  },
  stripe: {
    publicKey: process.env.STRIPE_PUBLIC_KEY || '',
    secretKey: process.env.STRIPE_SECRET_KEY || '',
  },
  rapidId: {
    apiUrl: process.env.RAPID_ID_API_URL || '',
    authToken: process.env.RAPID_ID_AUTH_TOKEN || '',
  },
  snsPushNotification: {
    region: 'us-east-1',
    apiVersion: '2010-03-31',
    snsAndroidArn: process.env.SNS_ANDROID_ARN || '',
    snsIosArn: process.env.SNS_IOS_ARN || '',
    accessKeyId: process.env.AWS_ACCESS_KEY || '',
    accessSecretKey: process.env.AWS_SECRET_KEY || '',
    topicArn: process.env.TOPIC_ARN || '',
  },
};

export { config };
