import { Context } from 'koa';
import { handleRequestStatus } from './request-status';
import { UserRole } from '../entity/user';

const adminVerifierPermission = {
  adminVerifier: UserRole.ADMIN_VERIFIER
};

const superAdminPermission = {
  superAdmin: UserRole.SUPER_ADMIN
};

const userPermission = {
    user: UserRole.USER
};

const permissionWithoutSuperAdmin = {
    user: UserRole.USER,
    adminVerifier: UserRole.ADMIN_VERIFIER,
    individualVerifier: UserRole.INDIVIDUAL_VERIFIER,
};

function checkPermission(allowed) {
  return function (ctx: Context, next) {
    if (!Object.values(allowed).includes(ctx.state.user.role)) {
      return handleRequestStatus(ctx, 405, 'Permission denied.');
    }
    return next();
  };
}

export { superAdminPermission, userPermission, adminVerifierPermission, permissionWithoutSuperAdmin, checkPermission };
