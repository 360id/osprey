import { Context } from 'koa';

export function handleRequestStatus(ctx: Context, status, message?: any, data?) {
  const body = {};
  body['data'] = data;
  body['message'] = message;
  ctx.status = status;
  ctx.body = body;
}

export const successResponse = (message?: string) => ({
  ok: true,
  message: message,
});
