import { awsInstance } from './create-aws-instance';


export const creteSnsTopic = (topicName) => {
    return new Promise((resolve, reject) => {
        awsInstance().createTopic({Name: 'identity'}, (err, data) => {
            if (err) {
               reject(err);
               return;
            }
            resolve(data.TopicArn);
        });
    });
};
