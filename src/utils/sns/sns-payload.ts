const buildAPSPayloadString = (message) => {
    return JSON.stringify({
        aps: {
            alert: message,
        }
    });
};
const buildFCMPayloadString = (message) => {
    return JSON.stringify({
        notification: {
            text: message
        }
    });
};

export { buildAPSPayloadString, buildFCMPayloadString };
