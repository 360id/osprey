import { config } from '../../config';
import AWS from 'aws-sdk';

export const awsInstance = () => {
    const SESConfig = {
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_KEY,
        region: config.snsPushNotification.region,
        apiVersion: config.snsPushNotification.apiVersion
    };
    AWS.config.update(SESConfig);
    return new AWS.SNS();
};
