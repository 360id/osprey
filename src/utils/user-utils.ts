import jwt from 'jsonwebtoken';
import { UserDto } from 'src/dto/user-dto';
const authConfig = require('../config').config;

export function generateToken(payload, expiryTime?: string) {
  /*
   * Generate JWT Token
   */
  return jwt.sign(payload, authConfig.jwtSecret, {
    expiresIn: expiryTime || authConfig.jwtExpiration,
  });
}

export function verifyToken(token: string) {
  try {
    const payload = jwt.verify(token, authConfig.jwtSecret);
    return payload;
  } catch (e) {
    console.error('Error decoding', e);
  }
}
