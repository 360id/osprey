import { ApolloServer } from 'apollo-server';
import 'reflect-metadata';
import { createSchema } from '../graphQLServer';
import { buildContext } from 'graphql-passport';
import * as entity from '../entity';
import * as services from '../services';

const createTestServer = async () => {
  const server = new ApolloServer({
    schema: await createSchema(),
    mockEntireSchema: false,
    mocks: true,
    context: async ({ req, res }) => buildContext({ req, res, entity, services }),
  });

  return { server, entity, services };
};

export { createTestServer };
