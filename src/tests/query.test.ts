import gql from 'graphql-tag';
import { createTestServer } from './helper';
import { createTestClient } from 'apollo-server-testing';
import { UserService } from '../services/UserService';
import { User } from '../entity/user';
jest.mock('../services/UserService');
jest.mock('../entity/user');

// const CURRENT_USER = gql`
//   query currentUser {
//     email
//   }
// `;

const REGISTER_USER = gql`
  mutation register($email: String!, $password: String!, $data: UserDataInput!) {
    register(input: { email: $email, password: $password, data: $data }) {
      ok
    }
  }
`;

describe('Auth', () => {
  //   test('should return current user', async () => {
  //     const { query } = await createTestServer({ user: { id: 1 } });

  //     const res = await query({ query: CURRENT_USER });
  //     expect(res).toMatchSnapshot();
  //   });

  it('should be able to register user', async () => {
    const { server, services } = await createTestServer();
    const { mutate } = createTestClient(server as any);
    services.UserService.getBy.mockReturnValueOnce([{ id: 1 }]);

    const res = await mutate({
      mutation: REGISTER_USER,
      variables: {
        email: 'test@test.com',
        password: 'test',
        data: { firstName: 'sonam', lastName: 'test', phoneNumber: 112131 },
      },
    });
    expect(res).toMatchSnapshot();
  });
});
