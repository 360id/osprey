// import { SwaggerRouter } from 'koa-swagger-decorator';

// const controller = require('./controller');
// import passport from 'koa-passport';

// const requireAuth = passport.authenticate('jwt', { session: false });

// const protectedRouter = new SwaggerRouter();
// import {
//     userPermission,
//     superAdminPermission,
//     checkPermission,
//     adminVerifierPermission,
//     permissionWithoutSuperAdmin
// } from './utils/user-permission-guard';

// // USER ROUTES
// protectedRouter.post('/user', requireAuth, checkPermission(adminVerifierPermission), controller.user.createUser);
// protectedRouter.get('/users', requireAuth, checkPermission(adminVerifierPermission), controller.user.getUsers);
// protectedRouter.get('/users/:id', requireAuth, checkPermission(adminVerifierPermission), controller.user.getUser);
// protectedRouter.put('/users/:id', requireAuth, checkPermission(adminVerifierPermission), controller.user.updateUser);
// protectedRouter.delete('/users/:id', requireAuth, checkPermission(adminVerifierPermission), controller.user.deleteUser);

// // ORGANIZATION ROUTES
// protectedRouter.post('/organization', requireAuth, checkPermission(superAdminPermission), controller.organization.createOrganization);
// protectedRouter.put('/organizations/:id', requireAuth, checkPermission(superAdminPermission), controller.organization.updateOrganization);
// protectedRouter.get('/organizations', requireAuth, checkPermission(superAdminPermission), controller.organization.getOrganizations);
// protectedRouter.get('/organizations/:id', requireAuth, checkPermission(superAdminPermission), controller.organization.getOrganization);
// protectedRouter.delete('/organizations/:id', requireAuth, checkPermission(superAdminPermission), controller.organization.deleteOrganization);
// protectedRouter.get('/organization-users/:id', requireAuth, checkPermission(superAdminPermission), controller.organization.getOrgUsers);

// // NOTIFICATIONS ROUTES
// protectedRouter.post('/notification', requireAuth, checkPermission(superAdminPermission), controller.notification.createNotification);
// protectedRouter.get('/notifications/:id', requireAuth, checkPermission(superAdminPermission), controller.notification.getNotification);
// protectedRouter.get('/notifications', requireAuth, checkPermission(superAdminPermission), controller.notification.getNotifications);
// protectedRouter.put('/notifications/:id', requireAuth, checkPermission(superAdminPermission), controller.notification.updateNotification);
// protectedRouter.delete('/notifications/:id', requireAuth, checkPermission(superAdminPermission), controller.notification.deleteNotification);

// protectedRouter.get('/user/notifications', requireAuth, controller.notification.getUserNotifications);

// // SNS
// protectedRouter.post('/create-sns-endpoint', requireAuth, controller.notification.createSnsEndpoint);
// protectedRouter.post('/push-notification', requireAuth, controller.notification.pushNotification);

// // CUSTOMER ROUTES
// protectedRouter.put('/customers/:id', requireAuth, checkPermission(superAdminPermission), controller.customer.updateCustomer);
// protectedRouter.get('/customers', requireAuth, checkPermission(superAdminPermission), controller.customer.getCustomers);
// protectedRouter.get('/customers/:id', requireAuth, checkPermission(superAdminPermission), controller.customer.getCustomer);
// protectedRouter.delete('/customers/:id', requireAuth, checkPermission(superAdminPermission), controller.customer.deleteCustomer);
// protectedRouter.post('/customer-support', requireAuth, checkPermission(permissionWithoutSuperAdmin), controller.customer.customerSupport);
// protectedRouter.post('/create-customer', requireAuth, checkPermission(superAdminPermission), controller.customer.createCustomer);

// // SCAN DOCUMENTS ROUTES
// protectedRouter.get('/scanDocuments', requireAuth, controller.scanDocument.getScanDocuments);
// protectedRouter.get('/scanDocuments/:id', requireAuth, controller.scanDocument.getScanDocument);
// protectedRouter.put('/scanDocuments/:id', requireAuth, controller.scanDocument.updateScanDocument);
// protectedRouter.delete('/scanDocuments/:id', requireAuth, controller.scanDocument.deleteScanDocument);

// // SUBSCRIPTION ROUTES
// protectedRouter.delete('/cancel-subscription', requireAuth, checkPermission(adminVerifierPermission), controller.subscription.cancelSubscription);

// // USER SETTINGS
// protectedRouter.put('/settings', requireAuth, checkPermission(userPermission), controller.customer.updateSettings);

// protectedRouter.post('/download-xlsx', requireAuth, checkPermission(superAdminPermission), controller.user.downloadXlsx);

// // PROFILE
// protectedRouter.post('/profile', requireAuth, controller.user.getProfile);

// // Swagger endpoint
// protectedRouter.swagger({
//     title: '360ID',
//     description: 'API REST that will facilitate transactions in various industries by expediting identity recognition.',
//     version: '0.1.0'
// });

// // mapDir will scan the input dir, and automatically call router.map to all Router Class
// protectedRouter.mapDir(__dirname);

// export { protectedRouter };
