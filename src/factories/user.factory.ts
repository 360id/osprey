import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import { User } from '../entity/user';

define(User, (faker: typeof Faker) => {
  const gender = faker.random.number(1);
  const firstName = faker.name.firstName(gender);
  const lastName = faker.name.lastName(gender);
  const email = faker.internet.email(firstName, lastName);

  const user = new User();

  user.email = email;
  user.firstName = firstName;
  user.lastName = lastName;
  user.password = faker.random.word();
  user.phoneNumber = faker.phone.phoneNumber();

  return user;
});
