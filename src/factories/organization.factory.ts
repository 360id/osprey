import * as Faker from 'faker';
import { define, factory } from 'typeorm-seeding';
import { User } from '../entity/user';
import { Organization } from '../entity/organization';

define(Organization, (faker: typeof Faker) => {
  const masterKey = faker.random.alphaNumeric();
  const businessName = faker.company.companyName();

  let organization: Organization = new Organization();
  organization.masterKey = masterKey;
  organization.businessName = businessName;
  organization.owner = factory(User)() as any;

  return organization;
});
