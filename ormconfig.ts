import { config } from './src/config';

module.exports = {
  type: 'postgres',
  host: config.databaseHost,
  port: config.databasePort,
  username: config.databaseUser,
  password: config.databasePassword,
  database: config.databaseName,
  migrationsTableName: config.migrationsTableName,
  migrations: config.migrations,
  cli: config.cli,
  seeds: config.seeds,
  entities: config.entities,
  factories: config.factories,
};
