# build stage
FROM node:14 AS base
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production  \
    && mv ./node_modules /tmp \
    && npm ci
COPY . .
RUN npm run build

# Runtime stage
FROM alpine:3.8 AS production
RUN apk add --update nodejs
RUN addgroup -S node && adduser -S node -G node
ENV NODE_ENV production
USER node

# RUN mkdir /usr/src/app
WORKDIR /usr/src/app

COPY --chown=node:node --from=base /usr/src/app /usr/src/app

COPY --chown=node:node --from=base /usr/src/app/build ./build

COPY --chown=node:node --from=base /tmp ./

CMD ["node", "build/src/server.js"]



